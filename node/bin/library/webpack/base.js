const { DefinePlugin } = require('webpack');
const { join } = require('path');
const { dllReferencePluginFactory } = require('./dll');
const loader = require('./loader');
const { packageRule, rule } = require('./rule');
const { ENVIRONMENT, VENDOR_PACKAGE_PATH } = require('../constants');

module.exports = {
  mode: ENVIRONMENT,
  module: {
    rules: [
      rule('js', [loader('babel')]),
      rule('svg', [loader('file')]),
      packageRule(['woff', 'woff2'], [loader('file')]),
    ],
  },
  plugins: [
    dllReferencePluginFactory('react'),
    dllReferencePluginFactory('ui'),
    new DefinePlugin({
      'process.env': {
        DEVELOPMENT: process.env.WEBPACK_BUILD_TARGET === 'development',
      },
    }),
  ],
  resolve: {
    // ZS-INFO:
    // Even though the DLL manifest is used,
    // the referenced modules must be installed
    // and resolvable by webpack.
    modules: [join(VENDOR_PACKAGE_PATH, 'node_modules')],
  },
  stats: 'none',
};
