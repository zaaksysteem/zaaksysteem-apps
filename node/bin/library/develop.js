const PROMPT_THRESHOLD = 1;
const serve = require('webpack-serve');
const mkdirp = require('mkdirp');
const { prompt } = require('inquirer');
const { readFileSync, writeFileSync } = require('fs');
const { join } = require('path');
const {
  APPS,
  CONTAINER_ROOT,
  NODE_ROOT,
  SERVER_DOCUMENT_ROOT,
  IS_STANDALONE,
  IS_DEVELOPMENT,
} = require('./constants');
const {
  DEVELOPMENT_HOST,
  DEVELOPMENT_PORT,
  INTERNAL_IP,
  WEBSOCKET_PORT,
} = require('./development');
const webpackConfigurationFactory = require('./webpack-configuration-factory');
const template = require('./template');

const { chdir } = process;

const read = baseName =>
  readFileSync(join(CONTAINER_ROOT, 'ssl', baseName), 'utf8');

/**
 * Certificate copied from another `zaaksysteem` volume.
 *
 * @type {Object}
 */
const https = (IS_DEVELOPMENT && IS_STANDALONE)
  ? undefined
  : {
    ca: read('ca.key'),
    cert: read('server.crt'),
    key: read('server.key'),
  };

/**
 * @see https://github.com/webpack-contrib/webpack-serve#options
 * @type {Object}
 */
const devMiddleware = {
  headers: {
    'Access-Control-Allow-Origin': '*',
  },
  stats: 'minimal',
};

/**
 * @see https://github.com/webpack-contrib/webpack-hot-client#options
 * @type {Object}
 */
const hotClient = {
  host: {
    // ZS-INFO: this generates a warning in STDOUT but is
    // necessary because the client browser on the host
    // requires the certificate's host name while the
    // container requires the internal IP address
    client: DEVELOPMENT_HOST,
    server: INTERNAL_IP,
  },
  port: WEBSOCKET_PORT,
  reload: false,
};

/**
 * Inquirer app choices.
 *
 * @type {Array<Object>}
 */
const choices = APPS
  .map(app => ({
    name: app.name,
    value: app,
  }));

/**
 * Inquirer questions.
 *
 * @see https://github.com/SBoudrias/Inquirer.js#questions
 * @type {Array<Object>}
 */
const questions = [
  {
    choices,
    default: null,
    message: 'Select an App to develop',
    name: 'app',
    type: 'list',
    when: () => choices.length > PROMPT_THRESHOLD,
  },
];

/**
 * Inquirer resolve function
 *
 * @param {Object} answers
 * @param {string} answers.app
 */
function start({ app: { name, source } }) {
  const appRoot = join(SERVER_DOCUMENT_ROOT, name);
  const indexPath = join(appRoot, 'index.html');
  const html = template({ id: name });

  mkdirp.sync(appRoot);
  writeFileSync(indexPath, html);

  // https://github.com/webpack-contrib/webpack-serve#argv
  const argv = {};
  // https://github.com/webpack-contrib/webpack-serve#options
  const options = {
    clipboard: false,
    config: webpackConfigurationFactory(name, source),
    devMiddleware,
    host: INTERNAL_IP,
    hotClient,
    https,
    port: DEVELOPMENT_PORT,
  };

  serve(argv, options);
}

chdir(NODE_ROOT);

/**
 * Start Inquirer, providing defaults to the
 * resolve function in case of skipped questions
 */
prompt(questions)
  .then(response => {
    const [defaultApp] = APPS;

    if (!response.app) {
      response.app = defaultApp;
    }

    start(response);
  });
