/**********************************************
 * Constants are tied to the file system
 * and the Node.js runtime environment.
 * They should only be required in executables,
 * never in library methods.
 */

const glob = require('glob');
const { join } = require('path');
const { getAppQueue } = require('./methods');

const {
  env: {
    CONTAINER_ROOT,
    NODE_ENV,
    NODE_ROOT,
    SERVER_ROOT,
    SOURCE_ROOT,
    VENDOR_ROOT,
  },
} = process;
const { assign } = Object;

/**
 * Apps are child directories of the source root
 * that contain a `manifest.json` file.
 *
 * @type {string}
 */
const manifestPattern = `${SOURCE_ROOT}/*/manifest.json`;

/**
 * Absolute paths to the app manifests.
 *
 * @type {Array}
 */
const manifestQueue = glob.sync(manifestPattern);

/**
 * Webpack build mode is 'development' (*how* bundles are compiled).
 *
 * @type {boolean}
 */
const IS_DEVELOPMENT = (NODE_ENV === 'development');

const IS_STANDALONE = process.env.STANDALONE ? true : false;

assign(exports, {
  APPS: getAppQueue(manifestQueue),
  CONTAINER_ROOT,
  ENVIRONMENT: IS_DEVELOPMENT
    ? NODE_ENV
    : 'production',
  IS_DEVELOPMENT,
  IS_STANDALONE,
  NODE_ROOT,
  SERVER_DOCUMENT_ROOT: SERVER_ROOT,
  SOURCE_ROOT,
  VENDOR_PACKAGE_PATH: VENDOR_ROOT,
  VENDOR_SERVER_PATH: join(SERVER_ROOT, 'vendor'),
});
