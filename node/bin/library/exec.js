const { spawn } = require('child_process');
const { NODE_ROOT } = require('./constants');

const EXIT_CODE_OK = 0;

/**
 * @param {string} binary
 * @param {Array<string>} [argumentList=[]]
 * @param {string} [cwd=NODE_ROOT]
 * @param {string} [env=process.env]
 * @return {Promise}
 */
const exec = (binary, argumentList = [], cwd = NODE_ROOT, env = process.env) =>
  new Promise(function executor(resolve, reject) {
    spawn(binary, argumentList, {
      cwd,
      env,
      stdio: 'inherit',
    }).on('close', function onClose(exitCode) {
      if (exitCode === EXIT_CODE_OK) {
        resolve(exitCode);
      } else {
        reject(exitCode);
      }
    });
  });

module.exports = exec;
