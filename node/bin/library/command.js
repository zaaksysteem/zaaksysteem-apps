const { join } = require('path');
const { existsSync } = require('fs');
const { cyan, green, yellow } = require('chalk');
const { prompt } = require('inquirer');
const { install, uninstall } = require('./npm');
const { error, info } = require('./methods');
const isValidPackage = require('./isValidPackage');
const exit = require('./exit');
const exec = require('./exec');
const { read, write } = require('./file');
const {
  CONTAINER_ROOT,
  IS_DEVELOPMENT,
  NODE_ROOT,
  SERVER_DOCUMENT_ROOT,
  SOURCE_ROOT,
  VENDOR_PACKAGE_PATH,
} = require('./constants');

const { keys } = Object;
const installationContexts = ['build', 'vendor'];
const SPLICE_LENGTH = 1;
const VENDOR_MANIFEST_PATH = `${NODE_ROOT}/vendor/package.json`;

const areDllManifestsMissing = () =>
  IS_DEVELOPMENT &&
  (!existsSync(join(SERVER_DOCUMENT_ROOT, 'vendor', 'react-manifest.json')) ||
    !existsSync(join(SERVER_DOCUMENT_ROOT, 'vendor', 'ui-manifest.json')));

function checkIntegrity() {
  if (areDllManifestsMissing()) {
    error('The DLL manifests do not exist.');
    error('This happens when `zs build` has been run.');
    error('You need to run the `./bin/vendor` command manually first.');
    process.exit();
  }
}

/**
 * Get the parsed vendor manifest file content.
 * You **cannot** use `require` here because modules are cached.
 *
 * @return {Object}
 */
const getVendorManifest = () => read(VENDOR_MANIFEST_PATH);

/**
 * @param {Object} data
 */
function setVendorManifest(data) {
  write(VENDOR_MANIFEST_PATH, data);
}

/**
 * @param {string} context
 * @return {boolean}
 */
const isValidContext = context => installationContexts.includes(context);

function rebuild() {
  exec('./bin/vendor').catch(() => {});
}

/**
 * Dictionary of spoofable packages in the `@mintlab` scope.
 * The first property becomes the default choice of the prompt.
 *
 * @type {Object}
 */
const spoofablePackages = {
  /**
   * @return {Promise}
   */
  '@mintlab/ui': function mintlabUi() {
    const cwd = join(CONTAINER_ROOT, 'ui');
    const target = join(VENDOR_PACKAGE_PATH, 'node_modules', '@mintlab', 'ui');
    const folder = 'distribution';

    return copyFiles({ cwd, target, folder });
  },
  '@mintlab/kitchen-sink': function mintlabKitchenSink() {
    const cwd = join(CONTAINER_ROOT, 'kitchen-sink');
    const target = join(
      VENDOR_PACKAGE_PATH,
      'node_modules',
      '@mintlab',
      'kitchen-sink'
    );

    return copyFiles({ cwd, target });
  },
};

/**
 * @param {Object} params
 * @param {string} params.cwd
 * @param {string} params.target
 * @param {string} [params.folder='.']
 * @param {string} [params.message='Copying distribution artefacts.']
 * @return {Promise}
 */
function copyFiles({
  cwd,
  target,
  folder = '.',
  message = 'Copying distribution artefacts.',
}) {
  info(message);

  return exec('cp', ['-rf', folder, target], cwd);
}

/**
 * @param {string} packageName
 */
function runSpoof(packageName) {
  spoofablePackages[packageName]().then(() =>
    exec('./bin/vendor').catch(() => {})
  );
}

function spoofPrompt() {
  const choices = keys(spoofablePackages);
  const [defaultChoice] = choices;
  const questions = [
    {
      choices,
      default: defaultChoice,
      message: 'Choose a spoofable package:',
      name: 'packageName',
      type: 'list',
    },
  ];

  /**
   * @param {Object} answers
   * @param answers.packageName
   */
  function use({ packageName }) {
    runSpoof(packageName);
  }

  prompt(questions).then(use);
}

/**
 * Replace an installed package in the `@mintlab` scope with
 * the distribution files of a mounted volume under
 * `/opt/zaaksysteem-apps/@mintlab`.
 * (NB: symbolic links appear not to be supported by the
 * webpack `file-loader` plugin.)
 *
 * @param {string} packageName
 */
function spoof(packageName) {
  if (!packageName) {
    info('No package name received.');
    spoofPrompt();
    return;
  }

  if (!Object.prototype.hasOwnProperty.call(spoofablePackages, packageName)) {
    info(`Package ${green(packageName)} cannot be spoofed.`);
    spoofPrompt();
    return;
  }

  runSpoof(packageName);
}

/**
 * @param {Function} command
 * @param {string} context
 */
function packagePrompt(command, context) {
  const questions = [
    {
      message: 'Provide a package name:',
      name: 'packageName',
      type: 'input',
    },
  ];

  /**
   * @param {Object} answers
   * @param {string} answers.packageName
   */
  function use({ packageName }) {
    command(context, packageName);
  }

  prompt(questions).then(use);
}

function contextPrompt(command) {
  const questions = [
    {
      choices: [
        {
          name: 'build: Node.js runtime environment packages',
          value: 'build',
        },
        {
          name: 'vendor: client-side packages',
          value: 'vendor',
        },
      ],
      message: 'Choose an installation context:',
      name: 'context',
      type: 'list',
    },
  ];

  function use({ context }) {
    command(context);
  }

  prompt(questions).then(use);
}

const getPackageBaseName = name => {
  const offset = name.lastIndexOf('@');
  const ZERO = 0;

  if (offset > ZERO) {
    return name.substring(ZERO, offset);
  }

  return name;
};

/**
 * @param {string} context
 * @param {string} packageName
 */
function addVendorPrompt(context, packageName) {
  const packageBaseName = getPackageBaseName(packageName);
  const vendorManifest = getVendorManifest();
  const { dll } = vendorManifest;
  const choices = keys(dll);
  const questions = [
    {
      choices,
      message: `Choose one or more stacks to add ${cyan(packageBaseName)}:`,
      name: 'presets',
      type: 'checkbox',
    },
  ];

  function use({ presets }) {
    if (!presets.length) {
      exit('no stack received');
    }

    info(`adding ${cyan(packageName)} to ${yellow(context)} context`);
    install(context, packageName)
      .then(function postInstall() {
        const vendorManifest = getVendorManifest();

        for (const stack of presets) {
          if (!vendorManifest.dll[stack].includes(packageBaseName)) {
            vendorManifest.dll[stack].push(packageBaseName);
            vendorManifest.dll[stack].sort();
          }
        }

        setVendorManifest(vendorManifest);
        rebuild();
      })
      .catch(function catchRejection(reason) {
        error(reason);
      });
  }

  prompt(questions).then(use);
}

function removeVendorPrompt(choices, callback) {
  const questions = [
    {
      choices,
      message: 'Choose the stacks to remove',
      name: 'stacks',
      type: 'checkbox',
    },
  ];

  /**
   * @param {Object} answers
   * @param {Array} answers.stacks
   */
  function use({ stacks }) {
    callback(stacks);
  }

  prompt(questions).then(use);
}

/**
 * @param {string} context
 * @param {string} packageName
 */
function removeVendor(context, packageName) {
  const vendorManifest = getVendorManifest();
  const { dll } = vendorManifest;

  const filterStacks = key => dll[key].includes(packageName);

  const choices = keys(dll).filter(filterStacks);

  /**
   * Remove the package name from the provided dll entries in the
   * vendor package manifest and uninstall the package if no entries
   * are left.
   *
   * @param {Array} stacks
   */
  function task(stacks) {
    const vendorManifest = getVendorManifest();
    const { dll } = vendorManifest;

    for (const key of stacks) {
      const value = dll[key];
      const index = value.indexOf(packageName);

      value.splice(index, SPLICE_LENGTH);
    }

    setVendorManifest(vendorManifest);

    if (choices.length === stacks.length) {
      uninstall(context, packageName).then(rebuild);
    }
  }

  /**
   * If there is only one stack, skip the prompt.
   */
  function updateStack() {
    if (choices.length === SPLICE_LENGTH) {
      task(choices);
    } else {
      removeVendorPrompt(choices, task);
    }
  }

  if (choices.length) {
    updateStack();
  } else {
    error(`package ${green(packageName)} not found in any stack`);
    info('running uninstall anyway to clean up');
    uninstall(context, packageName).then(rebuild);
  }
}

/**
 * Automatically prompt for missing argument values.
 *
 * @param {string} context
 * @param {string} packageName
 * @param {Function} command
 * @return {boolean}
 */
function autoPrompt(context, packageName, command) {
  if (!isValidContext(context)) {
    info(`no valid ${yellow('context')} received`);
    contextPrompt(command);

    return true;
  }

  if (!packageName) {
    info(`no valid ${green('package name')} received`);
    packagePrompt(command, context);

    return true;
  }

  return false;
}

/**
 * @param {string} context
 * @param {string} packageName
 */
function add(context, packageName) {
  if (!autoPrompt(context, packageName, add)) {
    if (!isValidPackage(context, packageName)) {
      exit(`the package ${green(packageName)} is not whitelisted`);
    }

    const map = {
      build() {
        info(`adding ${cyan(packageName)} to ${yellow(context)} context`);
        install(context, packageName).catch(function catchRejection(reason) {
          error(reason);
        });
      },
      vendor() {
        addVendorPrompt(context, packageName);
      },
    };

    map[context]();
  }
}

/**
 * @param {string} context
 * @param {string} packageName
 */
function remove(context, packageName) {
  if (!autoPrompt(context, packageName, remove)) {
    info(`removing ${cyan(packageName)} from ${yellow(context)} context`);

    const map = {
      build() {
        uninstall(context, packageName).catch(function catchRejection(reason) {
          error(reason);
        });
      },
      vendor() {
        removeVendor(context, packageName);
      },
    };

    map[context]();
  }
}

module.exports = {
  add,
  remove,
  spoof,

  update() {
    info('updating all dependencies');
    info(`step 1/2: ${yellow('build')} context`);
    return install('build').then(() => {
      info(`step 2/2: ${yellow('vendor')} context`);
      return install('vendor');
    });
  },

  start(buildTarget) {
    process.env.WEBPACK_BUILD_TARGET = buildTarget;
    checkIntegrity();
    exec('ln', [
      '-sf',
      `${SOURCE_ROOT}/Admin/legacy.css`,
      `${SERVER_DOCUMENT_ROOT}/admin/legacy.css`,
    ]);
    require('./develop');
  },

  build() {
    checkIntegrity();
    process.env.NODE_ENV = 'production';
    exec('./bin/build')
      .then(() => {
        // The legacy CSS file is not build with webpack and *might*
        // exist as a symlink in the development environment.
        exec('rm', ['-f', `${SERVER_DOCUMENT_ROOT}/admin/legacy.css`]);
        exec('cp', [
          '-i',
          `${SOURCE_ROOT}/Admin/legacy.css`,
          `${SERVER_DOCUMENT_ROOT}/admin/`,
        ]);
      })
      .catch(() => {});
  },

  lint(...rest) {
    exec('npx', ['eslint', './bin', './src', ...rest]).catch(() => {});
  },

  rtfm() {
    exec('npx', ['esdoc', '-c', './src/.esdoc']).then(() =>
      exec('npx', ['http-server'])
    );
  },

  test(...rest) {
    const env = Object.create(process.env);
    env.NODE_ENV = 'test';

    exec(
      'npx',
      ['jest', '--config=./src/test/jest.config.js', ...rest],
      undefined,
      env
    ).catch(() => {});
  },
};
