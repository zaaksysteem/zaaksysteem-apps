# `./node`

> This is the container working directory for build and development tasks.

## Coding style

You must use [Node.js modules](https://nodejs.org/docs/latest-v8.x/api/modules.html).
