import { getSegments } from '@mintlab/kitchen-sink';

export const inSearch = route => {
  if (!route) return false;
  const segments = getSegments(route) || [];
  const [, query] = route.split('?');
  return segments.includes('catalogus') && query;
};
