import { buildUrl } from '@mintlab/kitchen-sink';

/**
 * @param {string} id
 * @param {string} suffix
 * @param {boolean} shouldReturn
 * @param {Object} params
 * @return {string}
 */
export const getPathToCaseType = (id, suffix, shouldReturn, params = {}) =>
  buildUrl(`/admin/zaaktypen/${id}${suffix}`, {
    return_url: shouldReturn ? '/admin/catalogus/{}' : undefined,
    ...params,
  });

/**
 * @param {string} id
 * @param {string} suffix
 * @param {boolean} shouldReturn
 * @param {Object} params
 * @return {string}
 */
export const getPathToObjectType = (id, suffix, shouldReturn, params = {}) =>
  buildUrl(`/admin/objecttypen/${id}${suffix}`, {
    return_url: shouldReturn ? '/admin/catalogus/{}' : undefined,
    ...params,
  });

/**
 * @return {string}
 */
export const getPathToImport = () =>
  buildUrl('/admin/object/1', {
    return_url: '/admin/catalogus/{}',
  });

/**
 * Get path to catalog item
 * @param {string} id
 * @param {string} type
 * @return {string}
 */
export const getPathToItem = (id, type = 'folder') => {
  const basePath = '/admin/catalogus';
  if (!id) {
    return basePath;
  }

  const paths = {
    folder: `${basePath}/${id}`,
    case_type: getPathToCaseType(id, '/bewerken', true),
    object_type: getPathToObjectType(id, '/bewerken', true),
  };

  return paths[type];
};
