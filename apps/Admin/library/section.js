import { navigation } from '../Fixtures/navigation';

export const getSectionFromPath = path => {
  const found = navigation.find(item => path.includes(item.path));
  return found ? found.section : '';
};
