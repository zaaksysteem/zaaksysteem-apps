import LoadableConfiguration from './SystemConfiguration/SystemConfigurationContainer';
import LoadableLog from './Log/LogContainer';
import LoadableCatalog from './Catalog/CatalogContainer';
import { FEATURE_TOGGLE_CATALOG } from '../Fixtures/featureToggle';

/**
 * iFrame base segment dictionary.
 *
 * @type {Object}
 */
const routes = {
  // Main navigation.
  catalogus: FEATURE_TOGGLE_CATALOG ? LoadableCatalog : '/beheer/bibliotheek',
  gebruikers: '/medewerker',
  logboek: LoadableLog,
  transacties: '/beheer/sysin/transactions',
  koppelingen: '/beheer/sysin/overview',
  gegevens: '/beheer/object/datastore',
  configuratie: LoadableConfiguration,

  // Orphans. The URLs do *not* reflect their
  // position in the user interface hierarchy.
  import: '/beheer/import',
  objecttypen: '/beheer/objecttypen',
  object: '/beheer/object/import',
  search: '/beheer/object/search',
  woz: '/beheer/woz',
  zaaktypen: '/beheer/zaaktypen',
};

export default routes;
