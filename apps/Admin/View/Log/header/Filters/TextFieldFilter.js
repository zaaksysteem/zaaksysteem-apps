import React, { createElement } from 'react';
import { filterStyleSheet } from './Filter.style';
import { withStyles, GenericTextField, Icon } from '@mintlab/ui';

/**
 * @param {Object} props
 * @param {Object} props.classes
 * @param {string} props.name
 * @param {string} props.value
 * @param {string} props.startAdornmentName
 * @param {Function} props.endAdornmentAction
 * @param {string} props.placeholder
 * @param {Function} props.onChange
 * @param {Function} props.onKeyPress
 * @return {ReactComponent}
 */
export const TextFieldFilter = ({
  classes,
  name,
  value,
  startAdornmentName,
  endAdornmentAction,
  placeholder,
  onChange,
  onKeyPress,
}) => (
  <div className={classes.filterWrapper}>
    <GenericTextField
      name={name}
      value={value}
      startAdornment={createElement(Icon, {
        size: 'small',
        children: startAdornmentName,
      })}
      closeAction={value ? () => endAdornmentAction(name) : null}
      placeholder={placeholder}
      onChange={onChange}
      onKeyPress={onKeyPress}
      scope={`log-filter:${name}`}
    />
  </div>
);

export default withStyles(filterStyleSheet)(TextFieldFilter);
