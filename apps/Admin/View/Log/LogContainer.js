import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { translate } from 'react-i18next';
import { withFormik } from 'formik';
import Log from './Log';
import { fetchUsersList } from '../../Store/Log/Users/users.actions';
import {
  eventsSetRowsPerPage,
  eventsSetPage,
  eventsExport,
} from '../../Store/Log/Events/events.actions';
import { filtersApply } from '../../Store/Log/Filters/filters.actions';
import { AJAX_STATE_PENDING } from '../../../library/redux/ajax/createAjaxConstants';

const PAGE_CORRECTION = 1;

const formDefaults = {
  keyword: '',
  caseNumber: '',
  user: null,
};

/**
 * @param {string} value
 * @return {Array<number>}
 */
export const numericUiToNumbers = value =>
  value.split(/\s+/).map(token => Number(token));

/**
 * @param {Object} options
 * @param {string} options.keyword
 * @param {Number} options.caseNumber
 * @param {Object} options.user
 * @param {string} options.user.uuid
 * @returns {Boolean}
 */
export const shouldShowFilters = ({ keyword, caseNumber, user }) =>
  keyword !== '' || caseNumber !== '' || user.uuid !== '';

/**
 * @param {Object} state
 * @param {Object} state.log
 * @return {Object}
 */
const mapStateToProps = ({
  log: {
    events: { page, rowsPerPage, data, count, state },
    filters,
    users,
  },
}) => ({
  rowsPerPage,
  count,
  filters,
  loading: state === AJAX_STATE_PENDING,
  page: page ? page - PAGE_CORRECTION : 0,
  rows: data,
  userOptions: users.data,
  userOptionsLoading: users.state === AJAX_STATE_PENDING,
  showFilters: shouldShowFilters(filters),
  user: {
    ...filters.user,
    value: filters.user.uuid,
  },
});

/**
 * @param {Function} dispatch
 * @return {Object}
 */
const mapDispatchToProps = dispatch => ({
  exportLog: bindActionCreators(eventsExport, dispatch),
  applyFilters: bindActionCreators(filtersApply, dispatch),
  setPage: bindActionCreators(eventsSetPage, dispatch),
  setRowsPerPage: bindActionCreators(eventsSetRowsPerPage, dispatch),
  fetchUsers: query => dispatch(fetchUsersList(query)),
  clearFilters: () => dispatch(filtersApply({})),
});

/**
 * @param {Object} stateProps
 * @param {Object} dispatchProps
 * @param {Object} ownProps
 * @return {Object}
 */
const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { t } = ownProps;

  /**
   * @param {string} name
   * @return {Object}
   */
  const mapColumns = name => ({
    name,
    label: t(`log:column:${name}`),
  });

  const columns = ['caseId', 'date', 'description', 'component', 'user'].map(
    mapColumns
  );

  const noResultDescription = t('log:noResultDescription');
  const labelRowsPerPage = t('log:labelRowsPerPage');
  const headerTitle = t('log:title');
  const exportButtonTitle = t('log:exportButtonTitle');
  const keywordTranslation = t('log:filter:placeholder:keyword');
  const caseNumberTranslation = t('log:filter:placeholder:caseNumber');
  const userTranslations = {
    'form:choose': t('log:filter:placeholder:user'),
    'form:beginTyping': t('log:filter:beginTyping'),
    'form:loading': t('log:filter:loading'),
  };

  const rowsPerPageOptions = numericUiToNumbers('25 50 100');

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    caseNumberTranslation,
    columns,
    exportButtonTitle,
    keywordTranslation,
    userTranslations,
    headerTitle,
    labelRowsPerPage,
    noResultDescription,
    rowsPerPageOptions,
    formDefaults,
  };
};

const FormikWrappedLog = withFormik({
  enableReinitialize: true,
  mapPropsToValues: ({ filters: { keyword, caseNumber, user } }) => ({
    keyword,
    caseNumber,
    user: user.uuid
      ? {
          ...user,
          value: user.uuid,
        }
      : null,
  }),
})(Log);

/**
 * Connects {@link Log} with {@link i18next} and the store.
 *
 * @return {ReactElement}
 */
const LogContainer = translate()(
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  )(FormikWrappedLog)
);

export default LogContainer;
