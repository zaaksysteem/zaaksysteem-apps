import React from 'react';
import { LazyLoader } from '@mintlab/ui';

export const LoadableLog = props => (
  <LazyLoader
    promise={() =>
      import(
        // https://webpack.js.org/api/module-methods/#import
        /* webpackChunkName: "admin.log" */
        './LogContainer'
      )
    }
    {...props}
  />
);

export default LoadableLog;
