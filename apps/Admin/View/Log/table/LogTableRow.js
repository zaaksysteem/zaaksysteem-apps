import React from 'react';
import deepmerge from 'deepmerge';
import { TableRow, TableCell, withStyles } from '@mintlab/ui';
import { LinkCell, DateCell } from '../../Shared/Table/cells';
import { logStyleSheet } from '../Log.style';
import { sharedStylesheet } from '../../Shared/Shared.style';

/**
 * @reactProps {integer} caseId
 * @reactProps {string} component
 * @reactProps {string} date
 * @reactProps {string} description
 * @reactProps {Object} user
 * @reactProps {Object} classes
 *
 * @returns {ReactElement}
 */
const LogTableRow = ({
  caseId,
  component,
  date,
  description,
  user,
  classes,
  t,
  ...rest
}) => (
  <TableRow {...rest}>
    <TableCell className={classes.otherCells}>
      <LinkCell
        path={`/intern/zaak/${caseId}`}
        value={caseId}
        classes={{
          link: classes.caseIdCell,
        }}
      />
    </TableCell>
    <TableCell className={classes.dateCell}>
      <DateCell value={date} t={t} />
    </TableCell>
    <TableCell className={classes.descriptionCell}>{description}</TableCell>
    <TableCell className={classes.componentCell}>
      {t([`log:components:${component}`, `log:components:unknown`])}
    </TableCell>
    <TableCell className={classes.otherCells}>{user.displayName}</TableCell>
  </TableRow>
);

const mergedStyles = theme =>
  deepmerge(sharedStylesheet(theme), logStyleSheet(theme));

export default withStyles(mergedStyles)(LogTableRow);
