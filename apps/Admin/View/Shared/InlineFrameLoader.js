import React from 'react';
import { Loader, Render } from '@mintlab/ui';
import { extract } from '@mintlab/kitchen-sink';
import InlineFrame from './InlineFrame';
import styleSheet from './InlineFrameLoader.css';

/**
 * A loader component for {@link InlineFrame}.
 *
 * @param {Object} props
 * @return {ReactElement}
 */
export const InlineFrameLoader = props => {
  const [loading, iframeProps] = extract('loading', props);
  const visibility = loading ? 'hidden' : '';

  // ZS-INFO: The `InlineFrame` component cannot be a child
  // of the `Loader` component because it needs to be rendered
  // first in order to set the store's `loading` property.
  return (
    <div className={styleSheet.placeHolder}>
      <Render condition={loading}>
        <div className={styleSheet.loader}>
          <Loader active={true} />
        </div>
      </Render>
      <div
        className={styleSheet.wrapper}
        style={{
          visibility,
        }}
      >
        <InlineFrame {...iframeProps} />
      </div>
    </div>
  );
};
