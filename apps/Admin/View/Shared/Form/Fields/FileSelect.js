import React, { Component } from 'react';
import { FileSelect as UIFileSelect, FileList, File } from '@mintlab/ui';
import { sendFile } from '../../../../../library/requests/DocumentTemplate';
import { unique, cloneWithout } from '@mintlab/kitchen-sink';
import { STATUS_PENDING, STATUS_FAILED } from '../../Form/Constants/fileStatus';

/**
 * Renders a FileSelect from the mintlab/ui library, based
 * on the provided values array.
 *
 * Handles adding and removing files by calling onChange when needed.
 *
 * @reactProps {Array} accept
 * @reactProps {String} selectInstructions
 * @reactProps {String} dragInstructions
 * @reactProps {String} dropInstructions
 * @reactProps {String} orLabel
 * @reactProps {Array} ]value=[]]
 * @reactProps {Boolean} [multiple=true]
 * @reactProps {Function} onChange
 * @reactProps {String} name
 */
class FileSelect extends Component {
  constructor(props) {
    super(props);
  }

  /**
   * @returns {ReactElement}
   */
  render() {
    const {
      accept,
      selectInstructions,
      dragInstructions,
      dropInstructions,
      value = [],
      orLabel,
      multiple = true,
    } = this.props;

    return (
      <UIFileSelect
        onDrop={this.handleDrop}
        accept={accept}
        dragInstructions={dragInstructions}
        dropInstructions={dropInstructions}
        selectInstructions={selectInstructions}
        orLabel={orLabel}
        hasFiles={value && value.length}
        multiple={multiple}
        fileList={this.list()}
        error={this.error()}
      />
    );
  }

  /**
   * @return {ReactElement}
   */
  list = () => {
    const { value } = this.props;
    if (!value || !value.length) return null;

    const mapFiles = file => {
      const status = () =>
        file.status
          ? {
              status: file.status,
            }
          : {};

      return (
        <File
          key={file.key}
          name={file.name}
          {...status()}
          onDeleteClick={() => this.removeFile(file.key)}
        />
      );
    };

    return <FileList>{value.map(mapFiles)}</FileList>;
  };

  fileCompleted(key, response) {
    const { onChange, value, name } = this.props;
    const mapValue = thisValue => {
      return thisValue.key === key
        ? { ...cloneWithout(thisValue, 'status'), uuid: response.id }
        : thisValue;
    };

    onChange({
      target: {
        name,
        value: value.map(mapValue),
      },
    });
  }

  fileError(key) {
    const { onChange, value, name } = this.props;
    const mapValue = thisValue => {
      return thisValue.key === key
        ? { ...thisValue, status: STATUS_FAILED }
        : thisValue;
    };

    onChange({
      target: {
        name,
        value: value.map(mapValue),
      },
    });
  }

  removeFile(key) {
    const { onChange, value, name } = this.props;

    onChange({
      target: {
        name,
        value: value.filter(thisValue => thisValue.key !== key),
      },
    });
  }

  /**
   * @param {Array} acceptedFiles
   */
  handleDrop = acceptedFiles => {
    const { onChange, value, name, multiple } = this.props;

    if (!acceptedFiles || !acceptedFiles.length) return;
    if (!multiple && value.length) return;

    const fileList = multiple ? acceptedFiles : acceptedFiles.slice(0, 1);

    const fileObjects = fileList.map(file => {
      const key = unique();

      sendFile(file)
        .then(response => this.fileCompleted(key, response))
        .catch(error => this.fileError(key, error));

      return {
        key,
        uuid: null,
        name: file.name,
        status: STATUS_PENDING,
      };
    });

    onChange({
      target: {
        name,
        value: [...value, ...fileObjects],
      },
    });
  };

  /**
   * @return {Boolean}
   */
  error() {
    const { value } = this.props;
    const failedFiles = value.filter(
      thisFile => thisFile.status === STATUS_FAILED
    );
    return Boolean(failedFiles.length);
  }
}

export default FileSelect;
