import { TextField, Select, Checkbox } from '@mintlab/ui';
import {
  TEXT,
  SELECT,
  CHECKBOX,
  FILE_SELECT,
  FLATVALUE_SELECT,
  EMAIL,
  TEXTAREA,
} from '../Constants/fieldTypes';
import FlatValueSelect from '../FlatValueSelect';
import Textarea from './Textarea';
import FileSelect from './FileSelect';

export const FormFields = {
  [TEXT]: TextField,
  [SELECT]: Select,
  [FLATVALUE_SELECT]: FlatValueSelect,
  [CHECKBOX]: Checkbox,
  [EMAIL]: TextField,
  [SELECT]: Select,
  [TEXT]: TextField,
  [TEXTAREA]: Textarea,
  [FILE_SELECT]: FileSelect,
};
