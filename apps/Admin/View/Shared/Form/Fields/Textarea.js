import React from 'react';
import { withStyles, TextField } from '@mintlab/ui';
import { textareaStyleSheet } from './Textarea.style';

export const Textarea = ({ classes, ...restProps }) => (
  <TextField
    {...restProps}
    classes={{ inputRoot: classes.inputRoot, input: classes.textarea }}
  />
);

export default withStyles(textareaStyleSheet)(Textarea);
