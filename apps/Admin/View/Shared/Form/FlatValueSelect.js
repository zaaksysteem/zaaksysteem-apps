import React from 'react';
import { Select } from '@mintlab/ui';
import { get, asArray } from '@mintlab/kitchen-sink';

/**
Wraps Select, where the value may be provided as 
either an object or a flat value.

Always outputs the selected choice as a flat value.
**/

/**
 * @param {*} props
 * @return {ReactElement}
 */
const FlatValueSelect = props => {
  const { value, choices } = props;

  const getNormalizedValue = () => {
    if (typeof value === 'object') return value;
    if (!choices || !asArray(choices).length) return value;

    const foundChoice = choices.find(choice => choice.value === value);

    return foundChoice ? foundChoice : value;
  };

  const handleChange = event => {
    const { onChange } = props;

    const newEvent = {
      ...event,
      target: {
        ...event.target,
        value: get(event, 'target.value.value', null),
      },
    };

    onChange(newEvent);
  };

  return (
    <Select {...props} value={getNormalizedValue()} onChange={handleChange} />
  );
};

export default FlatValueSelect;
