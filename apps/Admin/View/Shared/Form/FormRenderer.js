import React, { Component } from 'react';
import { withFormik } from 'formik';
import { get, cloneWithout } from '@mintlab/kitchen-sink';
import { FormFields } from './Fields';
import { createValidation } from '../../../../library/validation/validation';
import defaultValidationMap from './Validation/validationMap';
import { withRules } from './Rules';

const getValuesFromDefinition = formDefinition =>
  formDefinition.reduce(
    (values, field) => ({
      ...values,
      [field.name]: field.value,
    }),
    {}
  );

const setTouchedAndHandleChange = ({
  setFieldTouched,
  handleChange,
}) => event => {
  const {
    target: { name },
  } = event;
  setFieldTouched(name, true);
  handleChange(event);
};

const Form = ({
  Fields,
  errors,
  children,
  touched,
  handleBlur,
  setFieldValue,
  setFieldTouched,
  handleChange,
  values,
  formDefinition,
  isValid,
}) => {
  const fields = formDefinition
    .filter(field => !field.hidden)
    .map(field => {
      const { name } = field;

      return {
        ...field,
        definition: field,
        FieldComponent: Fields[field.type],
        value: values[name],
        checked: values[name],
        error: touched[name] ? errors[name] : undefined,
        touched: touched[name],
        key: `field-${name}`,
        refValue: get(values, `${field.refValue}`, null),
        onChange: setTouchedAndHandleChange({
          setFieldTouched,
          handleChange,
        }),
        onBlur: handleBlur,
        setFieldValue,
      };
    });

  return children({
    fields,
    handleBlur,
    setFieldValue,
    setFieldTouched,
    handleChange,
    values,
    isValid,
    errors,
    touched,
  });
};

// eslint-disable valid-jsdoc
class FormRenderer extends Component {
  static defaultProps = {
    rules: [],
  };
  constructor(props) {
    super(props);
    this.state = {
      formDefinition: props.formDefinition,
    };

    const FormComponent =
      props.rules.length > 0
        ? withRules({
            Component: Form,
            setFormDefinition: this.setFormDefinition,
          })
        : Form;

    this.WrappedForm = withFormik({
      enableReinitialize: false,
      isInitialValid: this.props.isInitialValid,
      validate: this.validate,
      mapPropsToValues: () =>
        getValuesFromDefinition(this.state.formDefinition),
    })(FormComponent);
  }

  render() {
    const {
      props: { rules, children, FieldComponents = {}, ...rest },
    } = this;

    const Fields = {
      ...FormFields,
      ...FieldComponents,
    };

    const WrappedForm = this.WrappedForm;

    return (
      <WrappedForm
        Fields={Fields}
        formDefinition={this.state.formDefinition}
        rules={rules}
        {...cloneWithout(rest, 'formDefinition')}
      >
        {children}
      </WrappedForm>
    );
  }

  setFormDefinition = formDefinition => this.setState({ formDefinition });

  validate = values => {
    const {
      state: { formDefinition },
      props: { validationMap, t },
    } = this;

    const mergedValidationMap = {
      ...defaultValidationMap,
      ...validationMap,
    };

    const validation = createValidation({
      formDefinition,
      validationMap: mergedValidationMap,
      values,
      t,
    });

    return validation.then(yupErrors => {
      if (Object.keys(yupErrors).length === 0) return;

      const errors = yupErrors.inner.reduce((acc, error) => {
        const { message, path } = error;
        if (message && !acc[path]) {
          acc[path] = message;
        }
        return acc;
      }, {});

      if (errors) {
        throw errors;
      }
    });
  };
}

export default FormRenderer;
