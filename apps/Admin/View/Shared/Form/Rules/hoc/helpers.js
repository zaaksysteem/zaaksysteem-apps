import { executeRules } from '../library/executeRules';
import differenceWith from 'lodash/differenceWith';
import isEqual from 'lodash/isEqual';

export const getValuesFromDefinition = formDefinition =>
  formDefinition.reduce(
    (values, field) => ({
      ...values,
      [field.name]: field.value,
    }),
    {}
  );

export const getValuesDiff = (values, newValues) =>
  Object.keys(values).reduce((acc, key) => {
    if (values[key] !== newValues[key]) {
      return {
        ...acc,
        [key]: newValues[key],
      };
    }

    return acc;
  }, {});

export const isFormDefinitionUpdated = (
  formDefinition,
  newFormDefinition,
  values
) => {
  const formDefinitionWithValues = formDefinition.map(field => ({
    ...field,
    value: values[field.name],
  }));

  const formDefinitionDiff = differenceWith(
    formDefinitionWithValues,
    newFormDefinition,
    isEqual
  );

  return formDefinitionDiff.length > 0;
};

export const getFormDefinitionAfterRules = (rules, formDefinition, values) => {
  const formDefinitionWithValues = formDefinition.map(field => ({
    ...field,
    value: values[field.name],
  }));

  return rules.length > 0
    ? executeRules(rules, formDefinitionWithValues)
    : formDefinitionWithValues;
};
