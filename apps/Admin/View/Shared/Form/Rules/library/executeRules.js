export const executeRules = (rules, fields) => {
  return rules.reduce((accumulator, rule) => rule.execute(accumulator), fields);
};

export default executeRules;
