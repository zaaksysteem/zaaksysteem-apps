import { connect } from 'react-redux';
import { translate } from 'react-i18next';
import {
  openOverlay,
  closeOverlay,
  loadWindow,
  unloadWindow,
} from '../../../Store/UI/ui.actions';
import { invoke } from '../../../Store/Route/route.actions';
import Router from './Router';

const mapStateToProps = ({
  ui: {
    iframe: { loading, overlay },
  },
  route,
}) => ({
  hasIframeOverlay: overlay,
  isIframeLoading: loading,
  requestUrl: route,
});

const mapDispatchToProps = dispatch => ({
  onIframeOpen: () => dispatch(openOverlay()),
  onIframeClose: () => dispatch(closeOverlay()),
  onIframeLoad: () => dispatch(loadWindow()),
  onIframeUnload: () => dispatch(unloadWindow()),
  route: path => dispatch(invoke(path)),
});

const connectWithTranslation = translate();
const connectWithStore = connect(
  mapStateToProps,
  mapDispatchToProps
);

const RouterContainer = connectWithTranslation(connectWithStore(Router));

export default RouterContainer;
