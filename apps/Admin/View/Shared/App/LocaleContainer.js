import { connect } from 'react-redux';
import Locale from './Locale';
import { default as locale } from '../../../Fixtures/locale/index';

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps,
  locale,
});
const LocaleContainer = connect(
  null,
  null,
  mergeProps
)(Locale);

export default LocaleContainer;
