import React from 'react';
import { getSegment } from '@mintlab/kitchen-sink';
import { getUserNavigation } from '../../../../library/auth';
import { getUrl } from '../../../../library/url';
import { Loader } from '@mintlab/ui';

/**
 * @param {Object} props
 * @param {Array} props.navigation,
 * @param {Function} props.route,
 * @param {Object} props.session,
 * @return {ReactElement|null}
 */
export default function Login({
  navigation,
  route,
  capabilities,
  children,
  bootstrap,
}) {
  if (bootstrap !== 'valid' || capabilities === null) {
    return <Loader />;
  }

  const url = getUrl();
  const userNavigation = getUserNavigation(navigation, capabilities);

  if (!getSegment(url)) {
    const [{ path }] = userNavigation;

    route({
      path,
    });
  }

  return children;
}
