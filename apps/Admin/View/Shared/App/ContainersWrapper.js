import React, { Fragment } from 'react';
import SnackbarContainer from '../Snackbar/SnackbarContainer';
import DialogRendererContainer from '../DialogRenderer/DialogRendererContainer';

/**
 * @param {Object} props
 * @param {Object} props.children
 * @return {ReactElement}
 */
const ContainersWrapper = ({ children }) => (
  <Fragment>
    {children}
    <DialogRendererContainer />
    <SnackbarContainer />
  </Fragment>
);

export default ContainersWrapper;
