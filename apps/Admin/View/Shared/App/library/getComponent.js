import { InlineFrameLoader } from '../../../Shared/InlineFrameLoader';
import { objectifyParams } from '@mintlab/kitchen-sink';

/**
 * @param {string} url
 * @param {Object} routes
 * @return {Array}
 */
export default function getComponent(url, routes) {
  const [pathComponent, paramComponent] = url.split('?');
  const [, , segment, ...rest] = pathComponent.split('/');
  const params = objectifyParams(paramComponent);

  if (!Object.prototype.hasOwnProperty.call(routes, segment)) {
    return null;
  }

  const value = routes[segment];

  // iFrame with legacy src
  if (typeof value === 'string') {
    return [InlineFrameLoader, value];
  }

  // Original React component
  return [value, rest, params];
}
