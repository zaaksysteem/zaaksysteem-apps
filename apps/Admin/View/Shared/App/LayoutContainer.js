import { connect } from 'react-redux';
import { translate } from 'react-i18next';
import Layout from './Layout';
import { openDrawer, closeDrawer } from '../../../Store/UI/ui.actions';
import { logout } from '../../../Store/Session/session.actions';
import { getUserNavigation } from '../../../../library/auth';
import { navigation } from '../../../Fixtures/navigation';
import { invoke } from '../../../Store/Route/route.actions';

const mapStateToProps = ({
  ui: { drawer, banners },
  route,
  session: {
    data: {
      logged_in_user: { capabilities, initials, surname },
      account: {
        instance: { company },
      },
    },
  },
}) => ({
  isDrawerOpen: drawer,
  requestUrl: route,
  userName: `${initials} ${surname}`,
  userNavigation: getUserNavigation(navigation, capabilities),
  customer: company,
  banners,
});

const mapDispatchToProps = dispatch => ({
  logout: payload => dispatch(logout(payload)),
  closeDrawer: payload => dispatch(closeDrawer(payload)),
  openDrawer: payload => dispatch(openDrawer(payload)),
  route: path => dispatch(invoke(path)),
});

function mergeProps(stateProps, dispatchProps, ownProps) {
  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    toggleDrawer() {
      if (stateProps.isDrawerOpen) {
        dispatchProps.closeDrawer();
      } else {
        dispatchProps.openDrawer();
      }
    },
  };
}

const connectWithTranslation = translate();
const connectWithStore = connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
);

/**
 * The Layout Container Component is connected
 * with the store and the translations.
 *
 * @type {Function}
 */
const LayoutContainer = connectWithTranslation(connectWithStore(Layout));

export default LayoutContainer;
