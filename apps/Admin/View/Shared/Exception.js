import React, { Component, createRef } from 'react';
import { withStyles } from '@mintlab/ui';
import { exceptionStyleSheet } from './Exception.style';

/**
 * @reactProps {Object} classes
 * @reactProps {string} reason
 * @reactProps {string} trace
 */
class Exception extends Component {
  constructor(props) {
    super(props);
    this.domNode = createRef();
  }

  componentDidMount() {
    this.domNode.current.select();
  }

  /**
   * @return {ReactElement}
   */
  render() {
    const { classes } = this.props;

    return (
      <textarea
        className={classes.error}
        readOnly={true}
        ref={this.domNode}
        value={this.getValue()}
      />
    );
  }

  getValue() {
    const { reason, trace } = this.props;

    return [
      'Fatal Error',
      `URL:\n${document.location.href}`,
      `Reason:\n${reason}`,
      `Stacktrace:\n${trace}`,
    ].join('\n\n');
  }
}

export default withStyles(exceptionStyleSheet)(Exception);
