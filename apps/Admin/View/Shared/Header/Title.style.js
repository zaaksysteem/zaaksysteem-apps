/**
 * @param {Object} theme
 * @return {JSS}
 */
export const titleStylesheet = ({ typography }) => ({
  title: {
    fontWeight: typography.fontWeightMedium,
  },
});
