import React from 'react';
import { subAppHeaderStylesheet } from './SubAppHeader.style.js';
import { withStyles } from '@mintlab/ui';

/**
 * @reactProps {*} children
 * @reactProps {Object} classes
 * @return {ReactElement}
 */
export const SubAppHeader = ({ children, classes }) => (
  <header className={classes.header}>{children}</header>
);

export default withStyles(subAppHeaderStylesheet)(SubAppHeader);
