import React from 'react';
import { unique } from '@mintlab/kitchen-sink';
import {
  Dialog as UIDialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  DialogDivider,
} from '@mintlab/ui';
import createDialogActions from '../library/createDialogActions';

const getDialogActions = createDialogActions({
  primaryPresets: ['primary', 'contained'],
  secondaryPresets: ['default', 'contained'],
});

/**
 * Dialog used to display forms and large amount of data
 *
 * @param {Object} props
 * @param {boolean} [props.open=false]
 * @param {Object} [props.primaryButton]
 * @param {Object} [props.secondaryButton]
 * @param {String} title
 * @param {String} [scope]
 * @param {Function} [onClose]
 * @param {String} [icon]
 * @param {React.children} [children]
 * @return {ReactElement}
 */
export const Dialog = ({
  open = false,
  primaryButton,
  secondaryButton,
  title,
  scope,
  children,
  onClose,
  icon,
  ...rest
}) => {
  const labelId = unique();
  const dialogActions = getDialogActions(primaryButton, secondaryButton, scope);

  return (
    <UIDialog aria-labelledby={labelId} open={open} onClose={onClose} {...rest}>
      <DialogTitle
        elevated={true}
        icon={icon}
        id={labelId}
        title={title}
        onCloseClick={onClose}
        scope={scope}
      />
      <DialogContent padded={true}>{children}</DialogContent>
      {dialogActions && (
        <React.Fragment>
          <DialogDivider />
          <DialogActions>{dialogActions}</DialogActions>
        </React.Fragment>
      )}
    </UIDialog>
  );
};

export default Dialog;
