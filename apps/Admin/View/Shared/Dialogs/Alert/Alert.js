import React from 'react';
import { unique } from '@mintlab/kitchen-sink';
import { Dialog, DialogTitle, DialogContent, DialogActions } from '@mintlab/ui';
import createDialogActions from '../library/createDialogActions';

const getDialogActions = createDialogActions({
  primaryPresets: ['primary', 'text'],
  secondaryPresets: ['default', 'text'],
});

/**
 * Alert style Dialog
 *
 * @param {Object} props
 * @param {boolean} [props.open=false]
 * @param {Object} [props.primaryButton]
 * @param {Object} [props.secondaryButton]
 * @param {String} title
 * @param {String} [scope]
 * @param {React.children} [children]
 * @return {ReactElement}
 */
export const Alert = ({
  open = false,
  primaryButton,
  secondaryButton,
  title,
  scope,
  children,
  ...rest
}) => {
  const labelId = unique();
  const dialogActions = getDialogActions(primaryButton, secondaryButton, scope);

  return (
    <Dialog aria-labelledby={labelId} open={open} {...rest}>
      <DialogTitle id={labelId} title={title} scope={scope} />
      <DialogContent>{children}</DialogContent>
      {dialogActions && <DialogActions>{dialogActions}</DialogActions>}
    </Dialog>
  );
};

export default Alert;
