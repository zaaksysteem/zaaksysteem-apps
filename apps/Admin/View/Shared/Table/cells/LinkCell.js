import React from 'react';
import { Render, addScopeAttribute } from '@mintlab/ui';
import { cellStyleSheet } from './cells.style';
import { withStyles } from '@mintlab/ui';

/**
 * @param {Object} props
 * @param {Object} props.classes
 * @param {Function} props.handleNavigate
 * @param {string} props.value
 * @param {string} props.path
 * @param {string} props.scope
 * @return {ReactElement}
 */
export const LinkCell = ({ classes, handleNavigate, value, path, scope }) => (
  <Render condition={value}>
    <a
      href={path}
      className={classes.link}
      onClick={handleNavigate}
      {...addScopeAttribute(scope, 'link')}
    >
      {value}
    </a>
  </Render>
);

export default withStyles(cellStyleSheet)(LinkCell);
