import React from 'react';
import { Render } from '@mintlab/ui';
import ColoredIcon from './../../Icons/ColoredIcon';

/**
 * @param {Object} props
 * @param {string} props.value
 * @return {ReactElement}
 */
export const IconCell = ({ value }) => (
  <Render condition={value}>
    <ColoredIcon size="large" value={value} />
  </Render>
);

export default IconCell;
