import React from 'react';
import { translate } from 'react-i18next';
import { Card } from '@mintlab/ui';

/**
 * Generic error component for unresolvable routes.
 *
 * @return {ReactElement}
 */
export const ErrorNotFound = ({ t }) => (
  <Card title={t('common:routeNotFound')} />
);

export default translate()(ErrorNotFound);
