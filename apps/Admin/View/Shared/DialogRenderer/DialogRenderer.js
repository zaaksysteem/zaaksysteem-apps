import React from 'react';
import * as SystemConfigDialogs from '../../SystemConfiguration/Dialogs';
import * as CatalogDialogs from '../../Catalog/Dialogs';
import * as SharedDialogs from '../Dialogs';
import { withStyles } from '@mintlab/ui';
import { dialogRendererStylesheet } from './DialogRenderer.style';

const allDialogs = {
  ...SystemConfigDialogs,
  ...CatalogDialogs,
  ...SharedDialogs,
};

/**
 * Renders a specific dialog, based on the type defined in `ui.dialog.type`
 *
 * @param {Object} props
 * @return {ReactElement}
 */
const DialogRenderer = props => (
  <React.Fragment>
    {props.dialogs.map((dialog, index) => {
      const { type } = dialog;

      if (!type) return null;

      const Dialog = allDialogs[type];

      return <Dialog key={index} {...props} dialog={dialog} />;
    })}
  </React.Fragment>
);

export default withStyles(dialogRendererStylesheet)(DialogRenderer);
