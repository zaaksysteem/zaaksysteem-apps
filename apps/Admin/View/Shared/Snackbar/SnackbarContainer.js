import { connect } from 'react-redux';
import { translate } from 'react-i18next';
import SnackbarWrapper from './Snackbar';
import { hideSnackbar } from '../../../Store/UI/ui.actions';

const mapStateToProps = ({ ui: { snackbar } }) => ({ snackbar });
const mapDispatchToProps = dispatch => ({
  onQueueEmpty: payload => dispatch(hideSnackbar(payload)),
});

const SnackbarContainer = translate()(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(SnackbarWrapper)
);

export default SnackbarContainer;
