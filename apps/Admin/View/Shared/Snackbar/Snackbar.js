import React from 'react';
import { Snackbar } from '@mintlab/ui';
import { get } from '@mintlab/kitchen-sink';

/**
 * @param {Object} props
 * @param {Object} props.snackbar
 *   Snackbar configuration data
 * @param {Function} props.t
 *   `i18next` translation function
 * @param {Function} props.onQueueEmpty
 *   Action to dispatch when the Snackbar has finished its
 *   close transition, and there are no messages in the queue
 * @return {ReactElement}
 */
const SnackbarWrapper = ({ snackbar, t, onQueueEmpty }) => {
  const message = get(snackbar, 'message');
  const fallback = get(snackbar, 'fallback', '');

  if (!message) return null;

  return (
    <Snackbar message={t([message, fallback])} onQueueEmpty={onQueueEmpty} />
  );
};
export default SnackbarWrapper;
