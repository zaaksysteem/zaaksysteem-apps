import React from 'react';
import { Alert } from '../../Shared/Dialogs';
import { get } from '@mintlab/kitchen-sink';

const confirm = ({ dialog, discard }) => {
  const options = get(dialog, 'options');
  discard({
    ...options,
  });
};

/**
 * Discard Changes {@link DialogWrapper}.
 *
 * @param {Object} props
 * @param {Object} props.ui
 * @param {Function} props.t
 * @param {*} props.hide
 * @param {*} props.invoke
 * @return {ReactElement}
 */
const SystemConfigDiscardChanges = ({ dialog, t, hide, discard }) => (
  <Alert
    open={true}
    title={t('dialog:discardChanges:title')}
    icon="alarm"
    primaryButton={{
      text: t('dialog:ok'),
      onClick: () =>
        confirm({
          dialog,
          discard,
        }),
    }}
    secondaryButton={{
      text: t('dialog:cancel'),
      onClick: () => hide(),
    }}
    onClose={() => hide()}
  >
    {t('dialog:discardChanges:text')}
  </Alert>
);

export default SystemConfigDiscardChanges;
