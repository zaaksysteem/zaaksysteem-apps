import React from 'react';
import Categories from './Categories/Categories';
import FormikWrapper from './FormikWrapper';
import { systemConfigurationStyleSheet } from './SystemConfiguration.style.js';
import SubAppHeader from '../Shared/Header/SubAppHeader';
import Title from '../Shared/Header/Title';
import { withStyles, Loader } from '@mintlab/ui';

/**
 * @param {Object} props
 * @param {Function} props.invoke
 * @param {Array} props.categories
 * @param {Array} props.fieldSets
 * @param {Function} props.t
 * @param {Object} props.classes
 * @param {Array} props.changed
 * @param {Object} props.banners
 * @param {Function} props.showBanner
 * @param {Function} props.hideBanner
 * @param {boolean} props.discard
 * @param {Function} props.onDiscard
 * @return {ReactElement}
 */

export const SystemConfiguration = props => {
  const {
    classes,
    categories,
    invoke,
    t,
    fieldSets,
    banners,
    hideBanner,
    showBanner,
    save,
    discard,
    onDiscard,
  } = props;

  if (!fieldSets) {
    return <Loader />;
  }

  return (
    <div className={classes.wrapper}>
      <SubAppHeader>
        <Title>{t('systemConfiguration:title')}</Title>
      </SubAppHeader>

      <section className={classes.grid}>
        <Categories categories={categories} invoke={invoke} />

        <FormikWrapper
          fieldSets={fieldSets}
          banners={banners}
          t={t}
          hideBanner={hideBanner}
          showBanner={showBanner}
          save={save}
          onDiscard={onDiscard}
          discard={discard}
        />
      </section>
    </div>
  );
};

export default withStyles(systemConfigurationStyleSheet)(SystemConfiguration);
