# System Configuration

The System Configuration allows the end user to navigate through a number of categories, and displays form fields associated with each category. Making a change in a form field causes one of two banners to be displayed: 

- if there are no errors, the banner shows a save and undo button.
- if there are errors, the banners shows an undo button.

[Formik](https://jaredpalmer.com/formik/docs/overview) is used to manage the Form state.

The relevant component tree, on a high level, can be considered as: `SystemConfigurationWrapper → SystemConfigurationContainer → SystemConfiguration → FormikWrapper → Form`.

## SystemConfigurationWrapper

The backend API provides us with all field definitions and current values via `api/v1/config/panel`. This is loaded in via the System Configuration middleware, and after passing through the reducer, which transforms the raw data into a manageable array of simplified objects, it is provided as props to `SystemConfigurationContainer`.

We also load a static JSON file from `Fixtures`, containing details about the categorization, grouping and ordering of the form fields.

## SystemConfigurationContainer

Since the backend data is a flat list without knowledge of presentation, we create a full picture of the System Configuration, the Full Map, by merging the presentation structure with the field data objects. From this, a `fieldSets` objects is extracted, representing the current category's fields, which is also passed down.

## SystemConfiguration

This renders the header, the list of categories and Form, passing in props where needed.

## FormikWrapper

FormikWrapper uses the provided fieldSets from SystemConfigurationContainer to initialize itself, and acts as a wrapper around `Form`. Props from Formik and `SystemConfigurationContainer` are merged and passed down.

## Form

This is where the actual form fields are rendered using `fieldSets`. When the user changes a field's value, Formik's `setFieldValue` is called with the new value. This causes Formik to perform validations and pass down the new values,errors and more props, triggering reflows where necessary. The form fields are **controlled**, and thus will reflect their new value via the `value` prop.

After saving, the new values are synced with the items in the store, causing `FormikWrapper` to receive a new `InitialValues` object. This causes the form to reset itself with these new values.