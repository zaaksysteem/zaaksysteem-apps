import React from 'react';
import { LazyLoader } from '@mintlab/ui';

export const LoadableSystemConfiguration = props => (
  <LazyLoader
    promise={() =>
      import(
        // https://webpack.js.org/api/module-methods/#import
        /* webpackChunkName: "admin.system-configuration" */
        './SystemConfigurationContainer'
      )
    }
    {...props}
  />
);

export default LoadableSystemConfiguration;
