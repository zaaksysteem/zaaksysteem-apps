import React from 'react';
import { render } from 'react-dom';
import { Provider as StoreProvider } from 'react-redux';
import { MaterialUiThemeProvider } from '@mintlab/ui';
import ErrorBoundary from './Shared/App/ErrorBoundary';
import LocaleContainer from './Shared/App/LocaleContainer';
import LoginContainer from './Shared/App/LoginContainer';
import LayoutContainer from './Shared/App/LayoutContainer';
import RouterContainer from './Shared/App/RouterContainer';
import { createStore } from '../Store';
import { onPopState } from '../../library/dom/history';
import { getUrl } from '../../library/url';
import { resolve } from '../Store/Route/route.actions';
import { init } from '../Store/App/app.actions';
import routes from './routes';

import '@mintlab/ui/distribution/ui.css';
import './render.css';

const initialState = {};
const store = createStore(initialState);

function dispatchRoute() {
  store.dispatch(
    resolve({
      path: getUrl(),
    })
  );
}

store.dispatch(init());

onPopState(dispatchRoute);

const AdminApp = () => (
  <StoreProvider store={store}>
    <MaterialUiThemeProvider>
      <ErrorBoundary>
        <LocaleContainer>
          <LoginContainer>
            <LayoutContainer>
              <RouterContainer routes={routes} />
            </LayoutContainer>
          </LoginContainer>
        </LocaleContainer>
      </ErrorBoundary>
    </MaterialUiThemeProvider>
  </StoreProvider>
);

const rootNode = document.getElementById('zs-app');

window.onerror = null;
render(<AdminApp />, rootNode);

if (module.hot) {
  module.hot.accept();
}
