import { connect } from 'react-redux';
import { hideDialog } from '../../../../Store/UI/ui.actions';
import ConfirmDelete from './ConfirmDelete';
import { deleteItem } from '../../../../Store/Catalog/DeleteItem/deleteItem.actions';
import { getSelectedItem } from '../../../../Store/Catalog/Items/items.selectors';

const mapStateToProps = state => ({
  selectedItem: getSelectedItem(state),
});

const mapDispatchToProps = dispatch => ({
  deleteItem: (item, reason) => dispatch(deleteItem(item, reason)),
  onCancel: () => dispatch(hideDialog()),
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { selectedItem } = stateProps;

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    onConfirm: reason => dispatchProps.deleteItem(selectedItem, reason),
  };
};

const ConfirmDeleteContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(ConfirmDelete);

export default ConfirmDeleteContainer;
