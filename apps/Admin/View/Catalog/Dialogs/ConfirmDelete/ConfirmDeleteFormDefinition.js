export const formDefinition = [
  {
    name: 'reason',
    type: 'text',
    value: '',
    required: true,
    label: 'catalog:delete:confirm:label',
    hint: 'catalog:delete:confirm:description',
    placeholder: 'catalog:delete:confirm:placeholder',
  },
];
