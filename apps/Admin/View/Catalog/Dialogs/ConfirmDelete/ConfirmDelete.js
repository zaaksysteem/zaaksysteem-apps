import React from 'react';
import { unique, cloneWithout } from '@mintlab/kitchen-sink';
import {
  FormControlWrapper,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  DialogDivider,
  addScopeProp,
} from '@mintlab/ui';

import FormRenderer from '../../../Shared/Form/FormRenderer';
import createDialogActions from '../../../Shared/Dialogs/library/createDialogActions';
import { formDefinition } from './ConfirmDeleteFormDefinition';

const getDialogActions = createDialogActions({
  primaryPresets: ['primary', 'contained'],
  secondaryPresets: ['default', 'contained'],
});

const renderField = ({ t, scope, values, doSave, isValid }) => ({
  FieldComponent,
  name,
  ...rest
}) => {
  const props = {
    ...cloneWithout(rest, 'type', 'mode'),
    ...addScopeProp(scope, 'component', name),
    compact: true,
    name,
    key: name,
    t,
    onKeyPress(event) {
      if (event.key.toLowerCase() === 'enter' && isValid) {
        doSave(values);
      }
    },
  };
  return (
    <FormControlWrapper {...props}>
      <FieldComponent {...props} />
    </FormControlWrapper>
  );
};

const ConfirmDelete = ({ onConfirm, onCancel, t }) => {
  const doSave = values => onConfirm(values.reason);
  const title = t('catalog:delete:confirm:title');
  const labelId = unique();
  const scope = 'catalog-delete-dialog';
  const translatedFormDefinition = formDefinition.map(field => ({
    ...field,
    label: t(field.label),
    placeholder: t(field.placeholder),
    hint: t(field.hint),
  }));

  return (
    <Dialog
      id={scope}
      open={true}
      title={title}
      onClose={onCancel}
      scope={scope}
      disableBackdropClick={true}
    >
      <DialogTitle
        elevated={true}
        icon="error_outline"
        id={labelId}
        title={title}
        onCloseClick={onCancel}
        scope={scope}
      />

      <FormRenderer
        formDefinition={translatedFormDefinition}
        t={t}
        isInitialValid={false}
      >
        {({ fields, values, isValid }) => {
          const formFields = fields.map(
            renderField({
              t,
              values,
              doSave,
              isValid,
              scope,
            })
          );
          const dialogActions = getDialogActions(
            {
              text: t('catalog:delete:confirm:ok'),
              disabled: !isValid,
              action: () => doSave(values),
            },
            {
              text: t('dialog:cancel'),
              action: () => onCancel(),
            },
            'catalog-folder-dialog'
          );
          return (
            <React.Fragment>
              <DialogContent padded={true}>{formFields}</DialogContent>
              <React.Fragment>
                <DialogDivider />
                <DialogActions>{dialogActions}</DialogActions>
              </React.Fragment>
            </React.Fragment>
          );
        }}
      </FormRenderer>
    </Dialog>
  );
};

export default ConfirmDelete;
