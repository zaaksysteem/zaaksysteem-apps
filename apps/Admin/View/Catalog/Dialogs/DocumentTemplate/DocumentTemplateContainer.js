import { connect } from 'react-redux';
import { translate } from 'react-i18next';
import { AJAX_STATE_PENDING } from '../../../../../library/redux/ajax/createAjaxConstants';
import { saveDocumentTemplate } from '../../../../Store/Catalog/DocumentTemplate/documentTemplate.actions';
import DocumentTemplate from './DocumentTemplate';
import { get, unique } from '@mintlab/kitchen-sink';
import uuidv4 from 'uuid/v4';
import formDefinition from '../../../../Fixtures/documentTemplate';

const { keys } = Object;

const mangleValuesForSave = ({ values, id, currentFolderUUID }) => {
  const isNew = !id;
  const documentTemplateId = isNew ? uuidv4() : id;

  // Modify values where needed, before they get
  // sent to the save action, and to the API endpoint.

  /* eslint complexity: [2, 11] */
  let fields = keys(values).reduce((accumulator, key) => {
    switch (key) {
      case 'integration_uuid':
        accumulator[key] = values[key] === 'default' ? null : values[key];
        return accumulator;
      default:
        accumulator[key] = values[key];
        return accumulator;
    }
  }, {});

  fields.category_uuid = currentFolderUUID;

  if (fields.file.length) {
    const [file] = fields.file;
    fields.file_uuid = file.uuid;
    delete fields.file;
  }

  return {
    fields,
    documentTemplateId,
    isNew,
  };
};

const mapStateToProps = (
  {
    catalog: {
      documentTemplate,
      documentTemplate: { integrations, values, id },
      items: { currentFolderUUID },
    },
  },
  { t }
) => {
  // Insert translations, requests and other values into FormDefinition
  const mode = id ? 'edit' : 'create';

  const mapFormDefinition = field => {
    const fileProps = () => {
      if (field.name !== 'file') return {};

      const props = [
        'selectInstructions',
        'dragInstructions',
        'dropInstructions',
        'orLabel',
      ].reduce((acc, current) => {
        acc[current] = t(field[current]);
        return acc;
      }, {});
      return props;
    };

    const choicesProps = () => {
      if (field.name !== 'integration_uuid') return {};
      const merged = [...field.choices, ...integrations];
      return {
        choices: merged.map(integration => ({
          value: integration.value,
          label: t(integration.label),
        })),
      };
    };

    return {
      ...field,
      label: t(field.label),
      placeholder: t(field.placeholder),
      hint: t(field.hint),
      help: t(field.help),
      get value() {
        const value = get(values, field.name) || field.value;

        switch (field.name) {
          case 'file':
            return get(values, 'file_uuid')
              ? [
                  {
                    uuid: get(values, 'file_uuid'),
                    name: get(values, 'file_name'),
                    key: unique(),
                  },
                ]
              : [];
          case 'integration_uuid':
            return value === null ? 'default' : value;
          case 'commit_message':
            return mode === 'create'
              ? t('catalog:defaultCreate')
              : t('catalog:defaultEdit');
          default:
            return value;
        }
      },
      get integrations() {
        switch (field.name) {
          case 'integration_uuid':
            return integrations;
          default:
            break;
        }
      },
      get disabled() {
        return mode === 'edit' && field.name === 'integration_uuid';
      },
      ...fileProps(),
      ...choicesProps(),
    };
  };
  const newProps = {
    ...documentTemplate,
    formDefinition: formDefinition.map(mapFormDefinition),
    currentFolderUUID,
  };

  return {
    initializing:
      documentTemplate.state === AJAX_STATE_PENDING ||
      documentTemplate.integrationsState === AJAX_STATE_PENDING,
    saving: documentTemplate.savingState === AJAX_STATE_PENDING,
    ...newProps,
  };
};

const mapDispatchToProps = dispatch => ({
  dispatchSave: params => dispatch(saveDocumentTemplate(params)),
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { currentFolderUUID, id } = stateProps;
  const { dispatchSave } = dispatchProps;

  return {
    ...stateProps,
    ...ownProps,
    saveAction: ({ values }) =>
      dispatchSave(
        mangleValuesForSave({
          values,
          currentFolderUUID,
          id,
        })
      ),
  };
};

const connectedDialog = translate()(
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  )(DocumentTemplate)
);

export default connectedDialog;
