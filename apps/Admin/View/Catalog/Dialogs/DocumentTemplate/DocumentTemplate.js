import React from 'react';
import { FormControlWrapper, Loader, Render, withStyles } from '@mintlab/ui';
import { cloneWithout } from '@mintlab/kitchen-sink';
import { formStylesheet } from '../Shared/Form.style';
import FormRenderer from '../../../Shared/Form/FormRenderer';
import createDialogActions from '../../../Shared/Dialogs/library/createDialogActions';
import { unique } from '@mintlab/kitchen-sink';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  DialogDivider,
} from '@mintlab/ui';
import {
  showFields,
  hideFields,
  setRequired,
  setOptional,
  updateField,
  Rule,
} from '../../../Shared/Form/Rules';
import { FILE_SELECT } from '../../../Shared/Form/Constants/fieldTypes';
import { STATUS_PENDING } from '../../../Shared/Form/Constants/fileStatus';
import allFilesValid from '../../../Shared/library/validations/allFilesValid';

const getDialogActions = createDialogActions({
  primaryPresets: ['primary', 'contained'],
  secondaryPresets: ['default', 'contained'],
});

const matchesIntegration = module => (value, field) => {
  const integration = field.integrations.find(
    thisIntegration => thisIntegration.value === value
  );
  return integration && integration.module === module;
};

const rules = t => [
  new Rule('integration_uuid')
    .when(value => value !== 'default')
    .then(showFields(['integration_reference']))
    .then(setRequired(['integration_reference']))
    .else(hideFields(['integration_reference']))
    .else(setOptional(['integration_reference'])),
  new Rule('integration_uuid')
    .when(value => value === 'default')
    .then(showFields(['file']))
    .else(hideFields(['file'])),
  new Rule('integration_uuid').when(matchesIntegration('xential')).then(
    updateField('integration_reference', {
      label: t('documentTemplate:fields.templateExternalName.labelXential'),
    })
  ),
  new Rule('integration_uuid').when(matchesIntegration('stuf_dcr')).then(
    updateField('integration_reference', {
      label: t('documentTemplate:fields.templateExternalName.labelStufDCR'),
    })
  ),
];

const validationMap = {
  [FILE_SELECT]: allFilesValid,
};

/*eslint-disable */
const renderField = ({ t, classes }) => ({ FieldComponent, name, ...rest }) => {
  const props = {
    ...cloneWithout(rest, 'type', 'mode'),
    classes,
    compact: true,
    name,
    key: `document-template-form-component-${name}`,
    t,
    scope: `document-template-form-component-${name}`,
  };

  return (
    <FormControlWrapper {...props}>
      <FieldComponent {...props} />
    </FormControlWrapper>
  );
};
/*eslint-enable */

/**
 * @return {ReactElement}
 */
const DocumentTemplate = ({
  t,
  classes,
  formDefinition,
  hide,
  saveAction,
  initializing,
  saving,
  id,
}) => {
  const doSave = values => saveAction({ values });
  const title = t('documentTemplate:dialog.title', {
    action: id ? t('common:edit') : t('common:create'),
  });
  const labelId = unique();

  return (
    <Dialog
      id="catalog-document-template-dialog"
      open={true}
      title={title}
      classes={classes}
      onClose={() => hide()}
      scope="catalog-document-template-dialog"
      disableBackdropClick={true}
    >
      <DialogTitle
        elevated={true}
        icon="insert_drive_file"
        id={labelId}
        title={title}
        classes={{
          rootElevated: classes.dialogTitle,
        }}
        onCloseClick={() => hide()}
      />

      <Render condition={initializing}>
        <Loader />
      </Render>

      <Render condition={!initializing}>
        <FormRenderer
          formDefinition={formDefinition}
          t={t}
          isInitialValid={id ? true : false}
          rules={rules(t)}
          validationMap={validationMap}
        >
          {({ fields, values, isValid }) => {
            const pendingFiles = () => {
              if (!values.file || !values.file.length) return false;
              return values.file.filter(
                thisValue => thisValue.status === STATUS_PENDING
              ).length;
            };

            const formFields = fields.map(
              renderField({
                classes,
                t,
              })
            );
            const dialogActions = getDialogActions(
              {
                text: t('dialog:save'),
                disabled: saving || !isValid || pendingFiles(),
                action: () => doSave(values),
              },
              {
                text: t('dialog:cancel'),
                action: () => hide(),
              },
              'catalog-document-template-dialog'
            );
            return (
              <React.Fragment>
                <DialogContent padded={true}>{formFields}</DialogContent>
                <React.Fragment>
                  <DialogDivider />
                  <DialogActions>{dialogActions}</DialogActions>
                </React.Fragment>
              </React.Fragment>
            );
          }}
        </FormRenderer>
      </Render>
    </Dialog>
  );
};

export default withStyles(formStylesheet)(DocumentTemplate);
