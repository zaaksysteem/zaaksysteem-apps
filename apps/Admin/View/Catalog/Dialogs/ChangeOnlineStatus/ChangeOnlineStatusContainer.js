import { connect } from 'react-redux';
import { translate } from 'react-i18next';
import ChangeOnlineStatus from './ChangeOnlineStatus';
import { changeOnlineStatus } from '../../../../Store/Catalog/ChangeOnlineStatus/changeOnlineStatus.actions';
import { AJAX_STATE_PENDING } from '../../../../../library/redux/ajax/createAjaxConstants';
import formDefinition from '../../../../Fixtures/changeOnlineStatus';
import { get } from '@mintlab/kitchen-sink';

const mapStateToProps = (
  {
    catalog: {
      changeOnlineStatus: { state },
    },
  },
  { t, dialog }
) => {
  const active = get(dialog, 'options.selectedItems[0].active');

  const mapFormDefinition = field => {
    return {
      ...field,
      label: t(field.label, {
        type: active
          ? t('changeOnlineStatus:offline')
          : t('changeOnlineStatus:online'),
      }),
      placeholder: t(field.placeholder),
      hint: t(field.hint),
      help: t(field.help),
      loadingMessage: t(field.loadingMessage),
    };
  };

  return {
    busy: state === AJAX_STATE_PENDING,
    formDefinition: formDefinition.map(mapFormDefinition),
  };
};

const mapDispatchToProps = dispatch => {
  return {
    dispatchChangeOnlineStatus(payload) {
      dispatch(changeOnlineStatus(payload));
    },
  };
};

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { dispatchChangeOnlineStatus } = dispatchProps;
  const active = get(ownProps, 'dialog.options.selectedItems[0].active');
  const id = get(ownProps, 'dialog.options.selectedItems[0].id');

  return {
    ...stateProps,
    ...ownProps,
    active,
    id,
    changeOnlineStatusAction(values) {
      dispatchChangeOnlineStatus({
        ...values,
        active: !active,
        id,
      });
    },
  };
};

const connectedDialog = translate()(
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  )(ChangeOnlineStatus)
);

export default connectedDialog;
