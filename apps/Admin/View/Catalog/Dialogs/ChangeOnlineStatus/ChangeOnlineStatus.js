import React from 'react';
import FormRenderer from '../../../Shared/Form/FormRenderer';
import { cloneWithout, unique } from '@mintlab/kitchen-sink';
import createDialogActions from '../../../Shared/Dialogs/library/createDialogActions';
import { formStylesheet } from '../Shared/Form.style';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  DialogDivider,
  FormControlWrapper,
  Loader,
  Render,
  withStyles,
} from '@mintlab/ui';

const getDialogActions = createDialogActions({
  primaryPresets: ['primary', 'contained'],
  secondaryPresets: ['default', 'contained'],
});

const renderField = ({ t, classes, values, doSave, isValid }) => ({
  FieldComponent,
  name,
  ...rest
}) => {
  const props = {
    ...cloneWithout(rest, 'type', 'mode'),
    classes,
    compact: true,
    name,
    key: `changeonlinestatus-form-component-${name}`,
    t,
    scope: `changeonlinestatus-form-component-${name}`,
    onKeyPress(event) {
      if (event.key.toLowerCase() === 'enter' && isValid) {
        doSave(values);
      }
    },
  };
  return (
    <FormControlWrapper {...props}>
      <FieldComponent {...props} />
    </FormControlWrapper>
  );
};

/**
 * Change active status of Case Type
 *
 * @param {Object} props
 * @param {Function} props.t
 * @param {Object} props.classes
 * @param {Object} props.formDefinition
 * @param {Function} props.hide
 * @param {Function} props.changeOnlineStatusAction
 * @param {boolean} busy
 * @param {boolean} active
 * @return {ReactElement}
 */
const ChangeOnlineStatus = ({
  t,
  classes,
  formDefinition,
  hide,
  changeOnlineStatusAction,
  busy,
  active,
}) => {
  const scope = 'catalog:changeOnlineStatus';
  const type = active
    ? t('catalog:changeOnlineStatus.offline')
    : t('catalog:changeOnlineStatus.online');
  const title = t('catalog:changeOnlineStatus.title', { type });
  const labelId = unique();
  const doSave = values => changeOnlineStatusAction({ values });

  return (
    <Dialog
      id={scope}
      open={true}
      title={title}
      classes={classes}
      onClose={() => hide()}
      scope={scope}
      disableBackdropClick={true}
    >
      <DialogTitle
        elevated={true}
        icon="extension"
        id={labelId}
        title={title}
        classes={{
          rootElevated: classes.dialogTitle,
        }}
        onCloseClick={() => hide()}
        scope={scope}
      />

      <Render condition={busy}>
        <Loader />
      </Render>

      <Render condition={!busy}>
        <FormRenderer
          formDefinition={formDefinition}
          t={t}
          isInitialValid={false}
        >
          {({ fields, values, isValid }) => {
            const formFields = fields.map(
              renderField({
                classes,
                t,
                values,
                doSave,
                isValid,
              })
            );
            const dialogActions = getDialogActions(
              {
                text: t('dialog:save'),
                disabled: busy || !isValid,
                action: () => doSave(values),
              },
              {
                text: t('dialog:cancel'),
                action: () => hide(),
              },
              'catalog-changeonlinestatus--dialog'
            );
            return (
              <React.Fragment>
                <DialogContent padded={true}>{formFields}</DialogContent>
                <React.Fragment>
                  <DialogDivider />
                  <DialogActions>{dialogActions}</DialogActions>
                </React.Fragment>
              </React.Fragment>
            );
          }}
        </FormRenderer>
      </Render>
    </Dialog>
  );
};

export default withStyles(formStylesheet)(ChangeOnlineStatus);
