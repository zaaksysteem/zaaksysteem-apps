import React, { Component } from 'react';
import CaseTypeVersion from './Components/CaseTypeVersion';
import { Render, Loader } from '@mintlab/ui';
import { unique } from '@mintlab/kitchen-sink';
import { withStyles, Dialog, DialogTitle } from '@mintlab/ui';
import { caseTypeVersionsStyleSheet } from './CaseTypeVersions.style';

/**
 * @return {ReactElement}
 */
class CaseTypeVersions extends Component {
  render() {
    const {
      t,
      classes,
      hide,
      initCaseTypeVersionsActivate,
      loading,
      case_type_id,
      versions,
    } = this.props;

    const title = t('caseTypeVersions:dialog.title');
    const labelId = unique();

    return (
      <Dialog
        disableBackdropClick={true}
        id="catalog-case-type-versions-dialog"
        open={true}
        title={title}
        onClose={() => hide()}
        scope="catalog-case-type-versions-dialog"
      >
        <DialogTitle
          elevated={true}
          icon="history"
          id={labelId}
          title={title}
          onCloseClick={() => hide()}
        />

        <Render condition={loading}>
          <Loader />
        </Render>

        <Render condition={!loading}>
          <div className={classes.dialogContent}>
            {versions.map((version, index) => (
              <CaseTypeVersion
                key={index}
                initCaseTypeVersionsActivate={initCaseTypeVersionsActivate}
                case_type_id={case_type_id}
                version={version}
                t={t}
              />
            ))}
          </div>
        </Render>
      </Dialog>
    );
  }
}

export default withStyles(caseTypeVersionsStyleSheet)(CaseTypeVersions);
