import React from 'react';
import {
  Avatar,
  Button,
  Card,
  ExpansionPanel,
  ExpansionPanelSummary,
  ExpansionPanelDetails,
  Icon,
  Render,
  withStyles,
} from '@mintlab/ui';
import { caseTypeVersionStyleSheet } from './CaseTypeVersion.style';
import { withFormatDate } from './../../../../Shared/App/withFormatDate';
import classNames from 'classnames';

const CaseTypeVersion = ({
  classes,
  formatDate,
  initCaseTypeVersionsActivate,
  case_type_id,
  version: { active, details, last_modified, version_id, version },
  t,
}) => {
  const lastModified = new Date(last_modified);
  const date = formatDate(lastModified, t('common:dates:dateFormatText'));
  const time = formatDate(lastModified, t('common:dates:timeFormat'));

  return (
    <ExpansionPanel
      classes={{ root: classes.panel, expanded: classes.expandedPanel }}
    >
      <ExpansionPanelSummary
        classes={{
          root: classNames(classes.panelSummary, {
            [classes.panelSummaryActive]: active,
          }),
          content: classes.summaryContent,
          expanded: classes.summaryExpanded,
          expandIcon: classes.summaryExpandIcon,
        }}
        expandIcon={<Icon>expand_more</Icon>}
      >
        <div className={classes.summaryWrapper}>
          <Avatar className={active ? classes.avatarActive : classes.avatar}>
            {version}
          </Avatar>
          <div className={classes.dateTime}>
            <Render condition={last_modified}>
              <div className={classes.date}>{date}</div>
              <div className={classes.time}>{`${time} ${t(
                'common:hour'
              )}`}</div>
            </Render>
          </div>
          <Render condition={!active}>
            <Button
              action={event => {
                event.stopPropagation();
                initCaseTypeVersionsActivate({
                  case_type_id,
                  version_id,
                  version,
                });
              }}
              classes={{ root: classes.activateButton }}
              scope={`catalog-header:button-bar:activate`}
              presets={['primary', 'semiContained']}
            >
              {t('caseTypeVersions:dialog.activate')}
            </Button>
          </Render>
        </div>
      </ExpansionPanelSummary>
      <ExpansionPanelDetails
        className={classNames({ [classes.detailsActive]: active })}
      >
        <Card
          classes={{
            card: classes.detailsCard,
            content: classes.detailsContent,
          }}
        >
          {details.map((detail, index) => {
            return (
              <div key={index} className={classes.detailWrapper}>
                <div className={classes.detailTitle}>{`${detail.title}:`}</div>
                <div className={classes.detailValue}>{detail.value}</div>
              </div>
            );
          })}
        </Card>
      </ExpansionPanelDetails>
    </ExpansionPanel>
  );
};

export default withStyles(caseTypeVersionStyleSheet)(
  withFormatDate(CaseTypeVersion)
);
