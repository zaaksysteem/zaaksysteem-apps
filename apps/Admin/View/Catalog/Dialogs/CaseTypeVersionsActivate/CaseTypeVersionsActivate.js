import React from 'react';
import { FormControlWrapper } from '@mintlab/ui';
import { cloneWithout } from '@mintlab/kitchen-sink';
import FormRenderer from '../../../Shared/Form/FormRenderer';
import createDialogActions from '../../../Shared/Dialogs/library/createDialogActions';
import { unique } from '@mintlab/kitchen-sink';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  DialogDivider,
} from '@mintlab/ui';

const getDialogActions = createDialogActions({
  primaryPresets: ['primary', 'contained'],
  secondaryPresets: ['default', 'contained'],
});

const renderField = ({ t, classes, values, doSave, isValid }) => ({
  FieldComponent,
  name,
  ...rest
}) => {
  const props = {
    ...cloneWithout(rest, 'type', 'mode'),
    classes,
    compact: true,
    name,
    key: `case-type-versions-activate-component-${name}`,
    t,
    scope: `case-type-versions-activate-form-component-${name}`,
    onKeyPress(event) {
      if (event.key.toLowerCase() === 'enter' && isValid) {
        doSave(values);
      }
    },
  };
  return (
    <FormControlWrapper {...props}>
      <FieldComponent {...props} />
    </FormControlWrapper>
  );
};

/**
 * @return {ReactElement}
 */
const CaseTypeVersionsActivate = ({
  t,
  classes,
  activating,
  formDefinition,
  hide,
  caseTypeVersionsActivate,
  case_type_id,
  version_id,
  versionToActivate,
}) => {
  const doSave = values =>
    caseTypeVersionsActivate({
      case_type_id,
      version_id,
      reason: values.reason,
    });
  const title = t('caseTypeVersions:dialog.titleActivate', {
    version: versionToActivate,
  });
  const labelId = unique();
  const scope = 'catalog-case-type-versions-activate-dialog';

  return (
    <Dialog
      disableBackdropClick={true}
      id={scope}
      open={true}
      title={title}
      classes={classes}
      onClose={() => hide()}
      scope={scope}
    >
      <DialogTitle
        elevated={true}
        icon="extension"
        id={labelId}
        title={title}
        onCloseClick={() => hide()}
        scope={scope}
      />

      <FormRenderer formDefinition={formDefinition} t={t}>
        {({ fields, values, isValid }) => {
          const formFields = fields.map(
            renderField({
              classes,
              t,
              values,
              doSave,
              isValid,
            })
          );
          const dialogActions = getDialogActions(
            {
              text: t('dialog:save'),
              disabled: activating || !isValid,
              action: () => doSave(values),
            },
            {
              text: t('dialog:cancel'),
              action: () => hide(),
            },
            'catalog-folder-dialog'
          );
          return (
            <React.Fragment>
              <DialogContent padded={true}>{formFields}</DialogContent>
              <React.Fragment>
                <DialogDivider />
                <DialogActions>{dialogActions}</DialogActions>
              </React.Fragment>
            </React.Fragment>
          );
        }}
      </FormRenderer>
    </Dialog>
  );
};

export default CaseTypeVersionsActivate;
