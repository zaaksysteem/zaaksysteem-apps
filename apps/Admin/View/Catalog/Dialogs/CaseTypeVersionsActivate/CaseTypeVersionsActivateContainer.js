import { connect } from 'react-redux';
import { translate } from 'react-i18next';
import FormikWrappedDialog from './CaseTypeVersionsActivate';
import { caseTypeVersionsActivate } from '../../../../Store/Catalog/CaseTypeVersions/caseTypeVersions.actions';
import { AJAX_STATE_PENDING } from '../../../../../library/redux/ajax/createAjaxConstants';
import formDefinition from '../../../../Fixtures/caseTypeVersionActivate';

const mapStateToProps = ({ catalog: { caseTypeVersions } }, { t }) => {
  const mapFormDefinition = field => {
    return {
      ...field,
      label: t(field.label),
      placeholder: t(field.placeholder),
      hint: t(field.hint),
      help: t(field.help),
      loadingMessage: t(field.loadingMessage),
      value: field.value,
    };
  };

  return {
    ...caseTypeVersions,
    activating: caseTypeVersions.activatingState === AJAX_STATE_PENDING,
    formDefinition: formDefinition.map(mapFormDefinition),
  };
};

const mapDispatchToProps = dispatch => {
  return {
    caseTypeVersionsActivate(payload) {
      dispatch(caseTypeVersionsActivate(payload));
    },
  };
};

const connectedDialog = translate()(
  connect(
    mapStateToProps,
    mapDispatchToProps,
    null
  )(FormikWrappedDialog)
);

export default connectedDialog;
