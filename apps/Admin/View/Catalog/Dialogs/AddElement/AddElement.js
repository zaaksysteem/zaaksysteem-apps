import React from 'react';
import { unique } from '@mintlab/kitchen-sink';
import {
  withStyles,
  Dialog,
  DialogTitle,
  DialogDivider,
  DialogContent,
  Body2,
  Icon,
  addScopeAttribute,
} from '@mintlab/ui';

import ColoredIcon from '../../../Shared/Icons/ColoredIcon';
import { addElementStylesheet } from './AddElement.style';

const Section = ({ actions, classes }) => (
  <div className={classes.elementWrapper}>
    {actions.map(({ action, type, title, icon }) => (
      <button
        className={classes.element}
        key={type}
        type="button"
        onClick={action}
        {...addScopeAttribute('catalog', 'add-element', 'button', type)}
      >
        <div className={classes.elementIcon}>
          {icon ? (
            <Icon size="large">{icon}</Icon>
          ) : (
            <ColoredIcon size="large" value={type} />
          )}
        </div>
        <div className={classes.elementTitle}>
          <Body2>{title}</Body2>
        </div>
      </button>
    ))}
  </div>
);

/**
 * @return {ReactElement}
 */
const AddElement = ({ t, classes, hide, sections }) => {
  const handleClose = () => hide();
  const title = t('catalog:addElement.title');
  const labelId = unique();
  const scope = 'catalog-add-element-dialog';

  return (
    <Dialog
      id={scope}
      open={true}
      classes={classes}
      onClose={handleClose}
      scope={scope}
    >
      <DialogTitle
        elevated={true}
        icon="add"
        id={labelId}
        title={title}
        onCloseClick={handleClose}
        scope={scope}
      />
      <DialogContent padded={false}>
        {sections.map((section, index) => (
          <React.Fragment key={index}>
            {index > 0 && (
              <div className={classes.dividerWrapper}>
                <DialogDivider />
              </div>
            )}
            <Section actions={section} classes={classes} />
          </React.Fragment>
        ))}
      </DialogContent>
    </Dialog>
  );
};

export default withStyles(addElementStylesheet)(AddElement);
