import React, { Component } from 'react';
import { FormControlWrapper, Loader, Render } from '@mintlab/ui';
import { cloneWithout } from '@mintlab/kitchen-sink';
import FormRenderer from '../../../Shared/Form/FormRenderer';
import createDialogActions from '../../../Shared/Dialogs/library/createDialogActions';
import { unique } from '@mintlab/kitchen-sink';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  DialogDivider,
} from '@mintlab/ui';
import DocumentAttributeSearcher from './Components/DocumentAttributeSearcher';

const getDialogActions = createDialogActions({
  primaryPresets: ['primary', 'contained'],
  secondaryPresets: ['default', 'contained'],
});

//Custom type mapping for the EmailTemplate Dialog
const typeMap = {
  DocumentAttributeSearcher,
};

/*eslint-disable */
const renderField = ({ t, classes }) => ({ FieldComponent, name, ...rest }) => {
  const props = {
    ...cloneWithout(rest, 'type', 'mode'),
    classes,
    compact: true,
    name,
    key: `email-template-form-component-${name}`,
    t,
    scope: `email-template-form-component-${name}`,
  };
  return (
    <FormControlWrapper {...props}>
      <FieldComponent {...props} />
    </FormControlWrapper>
  );
};
/*eslint-enable */

/**
 * @return {ReactElement}
 */
class EmailTemplate extends Component {
  render() {
    const { t, classes, id, loading, formDefinition, hide } = this.props;

    const title = t('emailTemplate:dialog.title', {
      action: id ? t('common:edit') : t('common:create'),
    });
    const initializing = loading;
    const labelId = unique();
    const scope = 'catalog-email-template-dialog';

    return (
      <Dialog
        id={scope}
        open={true}
        title={title}
        classes={classes}
        onClose={() => hide()}
        scope={scope}
        disableBackdropClick={true}
      >
        <DialogTitle
          elevated={true}
          icon="extension"
          id={labelId}
          title={title}
          onCloseClick={() => hide()}
          scope={scope}
        />

        <Render condition={initializing}>
          <Loader />
        </Render>

        <Render condition={!initializing}>
          <FormRenderer
            formDefinition={formDefinition}
            FieldComponents={typeMap}
            t={t}
            isInitialValid={id ? true : false}
          >
            {this.renderForm}
          </FormRenderer>
        </Render>
      </Dialog>
    );
  }

  renderForm = ({ fields, values, isValid }) => {
    const {
      t,
      classes,
      id,
      saving,
      hide,
      saveAction,
      currentFolderUUID,
    } = this.props;
    const doSave = saveValues =>
      saveAction({
        values: saveValues,
        id,
        currentFolderUUID,
      });
    const formFields = fields.map(
      renderField({
        classes,
        t,
      })
    );

    const dialogActions = getDialogActions(
      {
        text: t('dialog:save'),
        disabled: saving || !isValid,
        action: () => doSave(values),
      },
      {
        text: t('dialog:cancel'),
        action: () => hide(),
      },
      'catalog-email-template-dialog'
    );

    return (
      <React.Fragment>
        <DialogContent padded={true}>{formFields}</DialogContent>
        <React.Fragment>
          <DialogDivider />
          <DialogActions>{dialogActions}</DialogActions>
        </React.Fragment>
      </React.Fragment>
    );
  };
}

export default EmailTemplate;
