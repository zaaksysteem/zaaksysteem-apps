import React, { useState } from 'react';
import { DataProvider } from '@mintlab/ui';
import { searchDocumentAttributes } from '../../../../../../library/requests/EmailTemplate';
import { asArray } from '@mintlab/kitchen-sink';
import { Select } from '@mintlab/ui';

const fetchDocumentAttributes = input =>
  searchDocumentAttributes(input)
    .then(attributes =>
      asArray(attributes).map(attribute => ({
        value: attribute.id,
        label: attribute.attributes.name,
      }))
    )
    .catch(() => {});

const DocumentAttributeSearcher = ({ ...restProps }) => {
  const [input, setInput] = useState('');

  return (
    <DataProvider
      providerArguments={[input]}
      autoProvide={input !== ''}
      provider={fetchDocumentAttributes}
    >
      {({ data, busy }) => {
        const normalizedChoices = data || [];
        return (
          <Select
            {...restProps}
            choices={normalizedChoices}
            isClearable={true}
            loading={busy}
            getChoices={setInput}
          />
        );
      }}
    </DataProvider>
  );
};

export default DocumentAttributeSearcher;
