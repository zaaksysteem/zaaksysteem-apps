import * as yup from 'yup';
import { generateMagicString } from '../../../../../../library/requests/Attribute.js';
import deepEqual from 'fast-deep-equal';

let timeoutId = null;
const DELAY = 200;

const isValidMagicstring = ({ t, field }) => {
  const { required } = field;
  function validateFunc(value = '') {
    if (!value) {
      return false;
    }

    return new Promise(resolve => {
      if (timeoutId) {
        clearTimeout(timeoutId);
      }
      timeoutId = setTimeout(() => {
        resolve();
      }, DELAY);
    })
      .then(() => generateMagicString(value))
      .then(magicstring => {
        if (!deepEqual(magicstring, value)) {
          return this.createError({
            message: t('validations:string.invalidMagicString', {
              suggestion: magicstring,
            }),
            path: 'magic_string',
          });
        }
        return true;
      })
      .catch(() => {});
  }

  const base = yup.string();

  return required
    ? base.required().test('is-valid-magicstring', null, validateFunc)
    : base.test('is-valid-magicstring', null, validateFunc);
};

export default isValidMagicstring;
