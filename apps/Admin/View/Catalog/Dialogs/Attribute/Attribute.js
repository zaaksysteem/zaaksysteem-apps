import React, { Component } from 'react';
import { withStyles, FormControlWrapper, Loader, Render } from '@mintlab/ui';
import { cloneWithout } from '@mintlab/kitchen-sink';
import { formStylesheet } from '../Shared/Form.style';
import Types from './Components/Types/Types';
import Versions from './Components/Versions/Versions';
import Options from './Components/Options/Options';
import Name from './Components/Name/Name';
import FormRenderer from '../../../Shared/Form/FormRenderer';
import createDialogActions from '../../../Shared/Dialogs/library/createDialogActions';
import { unique } from '@mintlab/kitchen-sink';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  DialogDivider,
} from '@mintlab/ui';
import { FormFields } from '../../../Shared/Form/Fields/';
import * as validationRules from '../../../Shared/Form/Validation/validationRules';
import isValidMagicstring from './Validation/isValidMagicstring';
import {
  MAGICSTRING,
  PRODUCT_SELECT,
  LOCATION_SELECT,
  TYPES,
  OPTIONS,
  VERSIONS,
  NAME,
} from '../../../Shared/Form/Constants/fieldTypes';
import LocationSelect from './Components/LocationSelect';
import ProductSelect from './Components/ProductSelect';
import {
  showFields,
  hideFields,
  valueEquals,
  valueOneOf,
  Rule,
} from '../../../Shared/Form/Rules';

const getDialogActions = createDialogActions({
  primaryPresets: ['primary', 'contained'],
  secondaryPresets: ['default', 'contained'],
});

//Custom type mapping for the Attribute Dialog
const typeMap = {
  [TYPES]: Types,
  [OPTIONS]: Options,
  [VERSIONS]: Versions,
  [MAGICSTRING]: FormFields.text,
  [NAME]: Name,
  [LOCATION_SELECT]: LocationSelect,
  [PRODUCT_SELECT]: ProductSelect,
};

// Custom Yup validation mapping for the Attribute Dialog
const validationMap = {
  [TYPES]: validationRules.selectRule,
  [MAGICSTRING]: isValidMagicstring,
  [LOCATION_SELECT]: validationRules.selectRule,
  [PRODUCT_SELECT]: validationRules.selectRule,
};

/* eslint-disable */
const renderField = ({ t, classes }) => ({ FieldComponent, name, ...rest }) => {
  const props = {
    ...cloneWithout(rest, 'type', 'mode'),
    classes,
    compact: true,
    name,
    key: `attribute-form-component-${name}`,
    t,
    scope: `attribute-form-component-${name}`,
  };

  return (
    <FormControlWrapper {...props}>
      <FieldComponent {...props} />
    </FormControlWrapper>
  );
};
/* eslint-enable */

const rules = [
  new Rule('attribute_type')
    .when(valueOneOf(['checkbox', 'select', 'option']))
    .then(showFields(['attribute_values']))
    .else(hideFields(['attribute_values'])),

  new Rule('attribute_type')
    .when(valueEquals('file'))
    .then(
      showFields([
        'document_category',
        'document_trust_level',
        'document_origin',
      ])
    )
    .else(
      hideFields([
        'document_category',
        'document_trust_level',
        'document_origin',
      ])
    ),

  new Rule('attribute_type')
    .when(valueEquals('appointment'))
    .then(showFields(['appointment_location_id', 'appointment_product_id']))
    .else(hideFields(['appointment_location_id', 'appointment_product_id'])),

  new Rule('attribute_type')
    .when(valueOneOf(['numeric', 'select', 'text', 'text_uc', 'textarea']))
    .then(showFields(['type_multiple']))
    .else(hideFields(['type_multiple'])),
];

/**
 * @return {ReactElement}
 */
class Attribute extends Component {
  render() {
    const {
      t,
      classes,
      id,
      loading,
      formDefinition,
      hide,
      fetchingIntegrations,
    } = this.props;

    const title = t('attribute:dialog.title', {
      action: id ? t('common:edit') : t('common:create'),
    });
    const initializing = loading || fetchingIntegrations;
    const labelId = unique();
    const scope = 'catalog-attribute-dialog';

    return (
      <Dialog
        id={scope}
        open={true}
        title={title}
        classes={classes}
        onClose={() => hide()}
        scope={scope}
        disableBackdropClick={true}
      >
        <DialogTitle
          elevated={true}
          icon="extension"
          id={labelId}
          title={title}
          classes={{
            rootElevated: classes.dialogTitle,
          }}
          onCloseClick={() => hide()}
          scope={scope}
        />

        <Render condition={initializing}>
          <Loader />
        </Render>

        <Render condition={!initializing}>
          <FormRenderer
            formDefinition={formDefinition}
            validationMap={validationMap}
            FieldComponents={typeMap}
            rules={rules}
            t={t}
            isInitialValid={id ? true : false}
          >
            {this.renderForm}
          </FormRenderer>
        </Render>
      </Dialog>
    );
  }

  renderForm = ({ fields, values, isValid }) => {
    const {
      t,
      classes,
      id,
      saving,
      hide,
      saveAction,
      currentFolderUUID,
      appointment_interface_uuid,
    } = this.props;

    const doSave = saveValues =>
      saveAction({
        values: saveValues,
        id,
        currentFolderUUID,
        appointment_interface_uuid,
      });
    const formFields = fields.map(
      renderField({
        classes,
        t,
      })
    );

    const dialogActions = getDialogActions(
      {
        text: t('dialog:save'),
        disabled: saving || !isValid,
        action: () => doSave(values),
      },
      {
        text: t('dialog:cancel'),
        action: () => hide(),
      },
      'catalog-attribute-dialog'
    );

    return (
      <React.Fragment>
        <DialogContent padded={true}>{formFields}</DialogContent>
        <React.Fragment>
          <DialogDivider />
          <DialogActions>{dialogActions}</DialogActions>
        </React.Fragment>
      </React.Fragment>
    );
  };
}

export default withStyles(formStylesheet)(Attribute);
