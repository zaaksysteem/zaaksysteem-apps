import React, { Component } from 'react';
import deepEqual from 'fast-deep-equal';

/**
 * HOC, intended to be used around Select. If new `choices`
 * props come in, and one of these choices match the
 * id of the current value, set the field value
 * to the matched object.
 *
 * This handles cases where the choices are
 * fetched and set after mounting,
 * but a value (with a temporary label)
 * is already set.
 */
/**
 *
 * @param {ReactComponent} WrappedComponent
 * @return {ReactComponent}
 */
const withChoiceWatcher = WrappedComponent => {
  class withChoiceWatcherHOC extends Component {
    constructor(props) {
      super(props);
    }
    componentDidUpdate(prevProps) {
      const { setFieldValue, choices, value, name } = this.props;
      if (!value) return;
      if (!deepEqual(choices, prevProps.choices)) {
        const match = choices.find(choice =>
          deepEqual(choice.value, value.value)
        );
        if (!match) return;
        setFieldValue(name, match);
      }
    }
    render() {
      return <WrappedComponent {...this.props} />;
    }
  }

  return withChoiceWatcherHOC;
};

export default withChoiceWatcher;
