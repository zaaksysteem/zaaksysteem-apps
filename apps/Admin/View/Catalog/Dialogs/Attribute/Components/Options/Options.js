import React, { Component } from 'react';
import Option from './Option';
import OptionsList from './OptionsList';
import {
  Button,
  TextField,
  Render,
  InputAdornment,
  withStyles,
  Subtitle2,
  ErrorLabel,
} from '@mintlab/ui';
import { bind, unique, asArray } from '@mintlab/kitchen-sink';
import { DragDropContext, Draggable, Droppable } from 'react-beautiful-dnd';
import { optionsStylesheet } from './Options.style';

class Options extends Component {
  static defaultProps = {
    value: [],
  };
  constructor(props) {
    super(props);
    this.state = {
      newItem: '',
      showInactive: true,
      error: null,
    };
    bind(
      this,
      'onDragEnd',
      'addItem',
      'onKeyPress',
      'reOrder',
      'switchActive',
      'deleteItem',
      'toggleInactive'
    );
  }

  render() {
    const {
      props: { classes, value, t },
      state: { newItem, showInactive, error },
    } = this;

    const filteredItems = asArray(value).filter(filteredItem =>
      showInactive ? true : filteredItem.active
    );

    const hasInactiveItems = asArray(value).filter(thisItem => !thisItem.active)
      .length;

    return (
      <div className={classes.optionsWrapper}>
        <DragDropContext onDragEnd={this.onDragEnd}>
          <Droppable droppableId="attribute-options-droppable">
            {(provided, snapshot) => (
              <OptionsList provided={provided} snapshot={snapshot}>
                {filteredItems.map((item, index) => (
                  <Draggable key={item.id} draggableId={item.id} index={index}>
                    {(draggableProvided, draggableSnapshot) => (
                      <Option
                        item={item}
                        index={index}
                        provided={draggableProvided}
                        snapshot={draggableSnapshot}
                        onDelete={this.deleteItem}
                        onSwitchActive={this.switchActive}
                        t={t}
                      />
                    )}
                  </Draggable>
                ))}
                {provided.placeholder}
              </OptionsList>
            )}
          </Droppable>
        </DragDropContext>
        <TextField
          classes={{
            formControl: classes.addNew,
          }}
          name="attribute-options-new"
          value={newItem}
          placeholder={t('attribute:dialog.namePlaceholder')}
          onKeyPress={this.onKeyPress}
          onChange={({ target }) => this.setState({ newItem: target.value })}
          InputProps={{
            endAdornment: (
              <InputAdornment
                classes={{
                  root: classes.adornment,
                }}
              >
                <Button
                  action={this.addItem}
                  label={t('attribute:dialog.addOption')}
                  presets={['primary', 'medium', 'icon']}
                >
                  add
                </Button>
              </InputAdornment>
            ),
          }}
        />

        <Render condition={error}>
          <ErrorLabel label={error} classes={{ root: classes.errorLabel }} />
        </Render>

        <Render condition={asArray(value).length && hasInactiveItems}>
          <Button
            action={() => this.toggleInactive()}
            presets={['text']}
            classes={{
              root: classes.showHideButton,
            }}
            label={
              showInactive
                ? t('attribute:dialog.hideInactive')
                : t('attribute:dialog.showInactive')
            }
          >
            <Subtitle2
              classes={{
                root: classes.showHide,
              }}
            >
              {showInactive
                ? t('attribute:dialog.hideInactive')
                : t('attribute:dialog.showInactive')}
            </Subtitle2>
          </Button>
        </Render>
      </div>
    );
  }

  reOrder(startIndex, endIndex) {
    const { value } = this.props;

    const result = Array.from(value);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);

    return result;
  }

  onDragEnd(result) {
    const { setFieldValue, name } = this.props;

    if (!result.destination) {
      return;
    }

    const newItems = this.reOrder(
      result.source.index,
      result.destination.index
    );

    setFieldValue(name, newItems);
  }

  addItem() {
    const {
      props: { setFieldValue, value, name, t },
      state: { newItem },
    } = this;

    if (!newItem.trim()) {
      return;
    }

    if (
      value.find(
        thisItem => thisItem.value.toLowerCase() === newItem.toLowerCase()
      )
    ) {
      this.setState({
        error: t('attribute:dialog.duplicateName'),
      });

      return;
    }

    const newItems = [
      ...value,
      {
        id: unique(),
        value: newItem,
        active: true,
        isNew: true,
      },
    ];
    setFieldValue(name, newItems);
    this.setState({
      newItem: '',
    });
  }

  deleteItem(item) {
    const { setFieldValue, value, name } = this.props;
    const newItems = value.filter(thisItem => item.id !== thisItem.id);
    setFieldValue(name, newItems);
  }

  onKeyPress(event) {
    const { key } = event;

    this.setState({ error: null });

    if (key.toLowerCase() === 'enter') {
      this.addItem();
    }
  }

  switchActive(item) {
    const { setFieldValue, value, name } = this.props;
    const newItems = value.map(thisItem => {
      if (item.id === thisItem.id) {
        return {
          ...item,
          active: !thisItem.active,
        };
      }
      return thisItem;
    });
    setFieldValue(name, newItems);
  }

  toggleInactive() {
    this.setState({
      showInactive: !this.state.showInactive,
    });
  }
}

export default withStyles(optionsStylesheet)(Options);
