import React from 'react';
import { optionsListStylesheet } from './OptionsList.style';
import { withStyles } from '@mintlab/ui';
import classNames from 'classnames';

const OptionsList = ({ provided, snapshot, children, classes }) => {
  const [items] = children;

  return (
    <div
      {...provided.droppableProps}
      ref={provided.innerRef}
      className={classNames(classes.list, {
        [classes.hidden]: !items.length,
        [classes.draggingOver]: snapshot.isDraggingOver,
      })}
    >
      {children}
    </div>
  );
};

export default withStyles(optionsListStylesheet)(OptionsList);
