import React from 'react';
import { optionStylesheet } from './Option.style';
import { Icon, withStyles, Button, Switch, Render } from '@mintlab/ui';
import classNames from 'classnames';

const getItemStyle = (snapshot, style) => {
  const transition = `all 0.03s`;

  if (snapshot.isDropAnimating) {
    return {
      ...style,
      transition,
    };
  } else if (snapshot.isDragging) {
    return {
      ...style,
      transform: `rotate(3deg) ${style.transform}`,
      transition,
    };
  }

  return {
    ...style,
  };
};

const Option = ({
  classes,
  item,
  provided,
  snapshot,
  onDelete,
  onSwitchActive,
  t,
}) => {
  return (
    <div
      ref={provided.innerRef}
      {...provided.draggableProps}
      {...provided.dragHandleProps}
      className={classNames(classes.wrapper, {
        [classes.dragging]: snapshot.isDragging,
      })}
      style={getItemStyle(snapshot, provided.draggableProps.style)}
    >
      <Icon
        classes={{
          root: classes.handle,
        }}
      >
        drag_indicator
      </Icon>
      <Switch
        checked={item.active}
        onChange={() => onSwitchActive(item)}
        variant="iOS"
      />

      <div className={classes.label}>{item.value}</div>
      <Render condition={item.isNew}>
        <Button
          action={() => onDelete(item)}
          label={t('attribute:dialog.deleteOption')}
          presets={['primary', 'medium', 'icon']}
          classes={{
            root: classes.delete,
          }}
        >
          close
        </Button>
      </Render>
    </div>
  );
};

export default withStyles(optionStylesheet)(Option);
