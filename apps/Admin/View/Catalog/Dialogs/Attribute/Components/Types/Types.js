import React from 'react';
import FlatValueSelect from '../../../../../Shared/Form/FlatValueSelect';

const Types = props => {
  return (
    <FlatValueSelect
      {...props}
      menuPortalTarget={document.getElementById(props.dialogId)}
    />
  );
};

export default Types;
