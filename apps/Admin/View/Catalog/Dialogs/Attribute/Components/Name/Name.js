import React from 'react';
import { FormFields } from '../../../../../Shared/Form/Fields/';
import { callOrNothingAtAll, get } from '@mintlab/kitchen-sink';

const Name = props => {
  const handleBlur = event => {
    const {
      setFieldValue,
      value = '',
      requests: { generateMagicString },
      onBlur,
    } = props;

    if (!get(props, 'refValue')) {
      generateMagicString(value).then(magicstring =>
        setFieldValue('magic_string', magicstring)
      );
    }

    callOrNothingAtAll(onBlur, [event]);
  };

  return <FormFields.text {...props} onBlur={handleBlur} />;
};

export default Name;
