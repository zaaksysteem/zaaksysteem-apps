import React from 'react';
import classNames from 'classnames';
import deepmerge from 'deepmerge';
import { catalogStyleSheet } from './Catalog.style';
import { sharedStylesheet } from '../Shared/Shared.style';
import CatalogHeaderContainer from './CatalogHeader/CatalogHeaderContainer';
import CatalogTable from './table/CatalogTable';
import DetailView from './DetailView/DetailView';
import MoveBar from './MoveBar/MoveBar';
import { withStyles, Card, Loader, Render, Button } from '@mintlab/ui';

/**
 * @reactProps {Object} classes
 * @reactProps {Array} columns
 * @reactProps {Object} detailInfo
 * @reactProps {Function} doNavigate
 * @reactProps {boolean} loadingTable
 * @reactProps {boolean} loadingDetailView
 * @reactProps {Function} onRowNavigate
 * @reactProps {Array<Object>} rows
 * @reactProps {boolean} showDetailView
 * @reactProps {Function} t
 * @reactProps {Function} toggleDetailView
 * @reactProps {Array} moveItems
 * @reactProps {Function} clearMoveItems
 * @reactProps {Function} confirmMoveItems
 * @reactProps {boolean} movingItems
 * @reactProps {boolean} inSearch
 * @return {ReactElement}
 */
const Catalog = ({
  classes,
  columns,
  detailInfo,
  doNavigate,
  toggleItem,
  loadingTable,
  loadingDetailView,
  onRowNavigate,
  rows,
  showDetailView,
  t,
  toggleDetailView,
  moveItems,
  clearMoveItems,
  confirmMoveItems,
  movingItems,
  addElement,
  inSearch,
}) => (
  <div className={classes.wrapper}>
    <CatalogHeaderContainer doNavigate={doNavigate} t={t} />
    <div className={classes.contentWrapper}>
      <Render condition={!inSearch}>
        <div
          className={classNames(
            classes.addElementWrapper,
            moveItems.length && !showDetailView ? 'withMoveBanner' : null
          )}
        >
          <Button
            action={addElement}
            scope="catalog-actions"
            presets={['primary', 'fab']}
          >
            add
          </Button>
        </div>
      </Render>

      <div
        className={classNames(classes.sheet, {
          [classes.sheetWidth]: showDetailView,
        })}
      >
        <Card
          className={classNames(classes.tableWrapper, {
            [classes.tableWrapperMovingItems]: moveItems.length,
          })}
          classes={{
            content: classes.card,
          }}
        >
          <CatalogTable
            columns={columns}
            rows={rows}
            onRowNavigate={onRowNavigate}
            toggleItem={toggleItem}
            t={t}
          />
          {loadingTable && <Loader className={classes.loader} />}
        </Card>
        <Render condition={moveItems.length}>
          <MoveBar
            t={t}
            numToMove={moveItems.length}
            moveAction={() => confirmMoveItems()}
            cancelAction={() => clearMoveItems()}
            disabled={movingItems || inSearch}
          />
        </Render>
      </div>
      <Render condition={showDetailView}>
        <DetailView
          closeDetailView={toggleDetailView}
          detailInfo={detailInfo}
          loading={loadingDetailView}
          t={t}
        />
      </Render>
    </div>
  </div>
);

/**
 * @param {Object} theme
 * @return {JSS}
 */
const mergedStyles = theme =>
  deepmerge(sharedStylesheet(theme), catalogStyleSheet(theme));

export default withStyles(mergedStyles)(Catalog);
