import { addRowData, getRowById } from './CatalogContainer';
import { getPathToItem } from '../../library/catalog/pathGetters';

describe('The `CatalogContainer` module', () => {
  describe('The `addRowData` function', () => {
    const data = {
      id: '1',
      type: 'folder',
    };

    test('adds a `path` string to the row data', () => {
      const { path } = addRowData([], [])(data);
      expect(path).toEqual(getPathToItem(data.id));
    });

    test('adds a `isNavigable` boolean to the row data', () => {
      const { isNavigable } = addRowData([], [])(data);
      expect(isNavigable).toBeTruthy();
    });

    test('creates `selected` properties according to the `selected` array', () => {
      const selected = ['a', 'c'];
      const actual = [
        {
          ...addRowData(selected, [])({
            id: 'a',
            type: 'folder',
          }),
        },
        {
          ...addRowData(selected, [])({
            id: 'b',
            type: 'folder',
          }),
        },
        {
          ...addRowData(selected, [])({
            id: 'c',
            type: 'case_type',
          }),
        },
      ];

      expect(actual[0].selected).toEqual(true);
      expect(actual[1].selected).toEqual(false);
      expect(actual[2].selected).toEqual(true);
    });
  });

  describe('The `getRowById` function that', () => {
    test('`returns the row where the id matches the selected id`', () => {
      const rows = [
        {
          type: 'first',
          id: 'unique1',
        },
        {
          type: 'second',
          id: 'unique2',
        },
        {
          type: 'third',
          id: 'unique3',
        },
      ];
      const selected = 'unique2';
      const actual = getRowById(rows, selected);
      const expected = { type: 'second', id: 'unique2' };

      expect(actual).toEqual(expected);
    });
  });
});
