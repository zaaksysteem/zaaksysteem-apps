import React from 'react';
import { valuesStyleSheet } from './Values.style';
import { withStyles } from '@mintlab/ui';

/**
 * @reactProps {Object} classes
 * @reactProps {Object} value
 * @reactProps {Object} t
 * @return {ReactElement}
 */
const Default = ({ classes, value }) => (
  <div className={classes.default}>
    <span>{value}</span>
  </div>
);

export default withStyles(valuesStyleSheet)(Default);
