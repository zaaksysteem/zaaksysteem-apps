import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { translate } from 'react-i18next';
import ButtonBar from './ButtonBar';
import { toggleCatalogDetailView } from '../../../Store/Catalog/Details/details.actions';
import {
  catalogDuplicateCaseType,
  catalogEditCaseType,
  catalogEditObjectType,
  catalogExportCaseType,
} from '../../../Store/Catalog/ButtonBar/buttonBar.actions';
import { startMoveItems as startMoveItemsAction } from '../../../Store/Catalog/MoveItems/moveItems.actions.js';
import { initChangeOnlineStatus as initChangeOnlineStatusAction } from '../../../Store/Catalog/ChangeOnlineStatus/changeOnlineStatus.actions';
import { DIALOG_CHANGE_ONLINE_STATUS } from '../Dialogs/dialogs.constants';
import { initAttribute as initAttributeAction } from '../../../Store/Catalog/Attribute/attribute.actions';
import { initEmailTemplate as initEmailTemplateAction } from '../../../Store/Catalog/EmailTemplate/emailTemplate.actions';
import { initCaseTypeVersions as initCaseTypeVersionsAction } from '../../../Store/Catalog/CaseTypeVersions/caseTypeVersions.actions';
import { initFolder as initFolderAction } from '../../../Store/Catalog/Folder/folder.actions';
import { capitalize, get } from '@mintlab/kitchen-sink';
import { initDocumentTemplate as initDocumentTemplateAction } from '../../../Store/Catalog/DocumentTemplate/documentTemplate.actions';
import { requestDeleteItem } from '../../../Store/Catalog/DeleteItem/deleteItem.actions';

const filterButtons = button => button.condition;

/**
 * @param {Object} state
 * @param {Object} state.catalog
 * @return {Object}
 */
const mapStateToProps = ({
  catalog: {
    items: { items, selectedItems },
    details,
    moveItems: { items: moveItems },
  },
}) => ({
  selectedItems: selectedItems.map(selectedItem =>
    items.find(item => item.id === selectedItem)
  ),
  showDetailView: details.showDetailView,
  moveItems,
});

const mapDispatchToProps = dispatch => ({
  duplicateCaseType: bindActionCreators(catalogDuplicateCaseType, dispatch),
  editCaseType: bindActionCreators(catalogEditCaseType, dispatch),
  editObjectType: bindActionCreators(catalogEditObjectType, dispatch),
  deleteItem: bindActionCreators(requestDeleteItem, dispatch),
  exportCaseType: bindActionCreators(catalogExportCaseType, dispatch),
  toggleDetailView: bindActionCreators(toggleCatalogDetailView, dispatch),
  startMoveItems: items => dispatch(startMoveItemsAction(items)),
  initChangeOnlineStatus: bindActionCreators(
    initChangeOnlineStatusAction,
    dispatch
  ),
  initAttribute: bindActionCreators(initAttributeAction, dispatch),
  initEmailTemplate: bindActionCreators(initEmailTemplateAction, dispatch),
  initFolder: bindActionCreators(initFolderAction, dispatch),
  initCaseTypeVersions: bindActionCreators(
    initCaseTypeVersionsAction,
    dispatch
  ),
  initDocumentTemplate: bindActionCreators(
    initDocumentTemplateAction,
    dispatch
  ),
});

/**
 * @param {Object} stateProps
 * @param {Object} dispatchProps
 * @param {Object} ownProps
 * @return {Object}
 */
/* eslint complexity: [2, 12] */
const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { showDetailView, selectedItems, moveItems } = stateProps;
  const {
    toggleDetailView,
    duplicateCaseType,
    editCaseType,
    editObjectType,
    exportCaseType,
    startMoveItems,
    initChangeOnlineStatus,
    initAttribute,
    initEmailTemplate,
    initFolder,
    initCaseTypeVersions,
    initDocumentTemplate,
    deleteItem,
  } = dispatchProps;
  const { t } = ownProps;

  const selectedItem = selectedItems[0];
  const selectedItemType = get(selectedItem, 'type');
  const selectedItemActive = get(selectedItem, 'active');

  const singleSelected = selectedItems.length === 1;
  const selectedCaseType = singleSelected && selectedItemType === 'case_type';
  const selectedObjectType =
    singleSelected && selectedItemType === 'object_type';
  const editableType =
    singleSelected &&
    ['attribute', 'email_template', 'folder', 'document_template'].includes(
      selectedItemType
    );

  const actionButtons = [
    {
      type: 'edit',
      action: () => editCaseType(),
      condition: selectedCaseType,
      tooltip: t('catalog:buttonBar:edit'),
    },
    {
      type: 'edit',
      action: () => editObjectType(),
      condition: selectedObjectType,
      tooltip: t('catalog:buttonBar:edit'),
    },
    {
      type: 'file_copy',
      action: () => duplicateCaseType(),
      condition: selectedCaseType,
      tooltip: t('catalog:buttonBar:duplicate'),
    },
    {
      type: 'folder_move',
      action: () => startMoveItems(selectedItems),
      tooltip: t('catalog:buttonBar:move'),
      condition: selectedItems.length && !moveItems.length,
    },
    {
      type: 'edit',
      action() {
        const options = { options: selectedItem };
        const actions = {
          attribute: () => initAttribute(options),
          folder: () => initFolder(options),
          document_template: () => initDocumentTemplate(options),
          email_template: () => initEmailTemplate(options),
        };
        actions[selectedItemType]();
      },
      tooltip: t('catalog:buttonBar:edit'),
      condition: editableType,
    },
    {
      type: 'history',
      action() {
        initCaseTypeVersions({ id: selectedItem.id });
      },
      tooltip: t('catalog:buttonBar:caseTypeVersions'),
      condition: selectedCaseType,
    },
    {
      type: 'delete',
      action: () => deleteItem(),
      tooltip: t('catalog:buttonBar:delete'),
      condition: singleSelected,
    },
  ].filter(filterButtons);

  const advancedActionButtons = [
    {
      action: () => exportCaseType(),
      title: t('catalog:buttonBar:export'),
      condition: selectedCaseType,
    },
    {
      action() {
        initChangeOnlineStatus({
          type: DIALOG_CHANGE_ONLINE_STATUS,
          options: {
            selectedItems,
          },
        });
      },
      title: capitalize(
        t('catalog:changeOnlineStatus:menuTitle', {
          type:
            singleSelected && selectedItemActive
              ? t('catalog:changeOnlineStatus:offline')
              : t('catalog:changeOnlineStatus:online'),
        })
      ),
      condition: selectedCaseType,
    },
  ].filter(filterButtons);

  const permanentButtons = [
    {
      type: 'info',
      action: () => toggleDetailView(),
      active: showDetailView,
      tooltip: t('catalog:buttonBar:details'),
    },
  ];

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    actionButtons,
    advancedActionButtons,
    permanentButtons,
  };
};

/**
 * Connects {@link ButtonBar} with {@link i18next}, and
 * the store.
 * @return {ReactElement}
 */
const ButtonBarContainer = translate()(
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  )(ButtonBar)
);

export default ButtonBarContainer;
