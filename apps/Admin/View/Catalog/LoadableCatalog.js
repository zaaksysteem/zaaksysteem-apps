import React from 'react';
import { LazyLoader } from '@mintlab/ui';

export const LoadableCatalog = props => (
  <LazyLoader
    promise={() =>
      import(
        // https://webpack.js.org/api/module-methods/#import
        /* webpackChunkName: "admin.catalog" */
        './CatalogContainer'
      )
    }
    {...props}
  />
);

export default LoadableCatalog;
