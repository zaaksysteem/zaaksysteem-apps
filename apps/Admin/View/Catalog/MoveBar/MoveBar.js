import React from 'react';
import { moveBarStyleSheet } from './MoveBar.style';
import { withStyles } from '@mintlab/ui';
import { Button } from '@mintlab/ui';
import { H5, Caption } from '@mintlab/ui';
import classNames from 'classnames';

const SCOPE = 'catalog:move-bar';

/**
 * @param {Function} cancelAction
 * @param {Function} moveAction
 * @param {*} numToMove
 * @param {Function} t
 * @param {boolean} [disabled=false]
 * @return {ReactElement}
 */
const MoveBar = ({
  classes,
  t,
  numToMove,
  moveAction,
  cancelAction,
  disabled = false,
}) => (
  <div className={classes.growWrapper}>
    <div className={classes.wrapper} data-scope={SCOPE}>
      <div className={classNames(classes.left)}>
        <H5 classes={{ root: classNames(classes.label, classes.labelTitle) }}>
          {t('catalog:move:label', { numToMove, count: numToMove })}
        </H5>
        <Caption
          classes={{
            root: classNames(classes.label, classes.labelDescription),
          }}
        >
          {t('catalog:move:description', { count: numToMove })}
        </Caption>
      </div>
      <div className={classes.right}>
        <Button
          action={cancelAction}
          presets={['text']}
          classes={{
            root: classes.buttonCancel,
          }}
          scope={`${SCOPE}:cancel`}
        >
          {t('dialog:cancel')}
        </Button>
        <Button
          action={moveAction}
          presets={['text', 'primary']}
          icon={'folder_move'}
          iconSize="small"
          classes={{
            root: classes.buttonMove,
          }}
          scope={`${SCOPE}:move`}
          disabled={disabled}
        >
          {t('catalog:move:moveButton')}
        </Button>
      </div>
    </div>
  </div>
);

export default withStyles(moveBarStyleSheet)(MoveBar);
