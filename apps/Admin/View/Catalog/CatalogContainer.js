import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { translate } from 'react-i18next';
import { filterProperties } from '@mintlab/kitchen-sink';
import Catalog from './Catalog';
import { showDialog } from '../../Store/UI/ui.actions';
import { toggleCatalogItem } from '../../Store/Catalog/Items/items.actions';
import {
  fetchCatalogItem,
  toggleCatalogDetailView,
} from '../../Store/Catalog/Details/details.actions';
import { AJAX_STATE_PENDING } from '../../../library/redux/ajax/createAjaxConstants';
import { invoke } from '../../Store/Route/route.actions';
import { getPathToItem } from '../../library/catalog/pathGetters';
import { inSearch } from '../../library/catalog/inSearch';
import {
  clearMoveItems as clearMoveItemsAction,
  confirmMoveItems as confirmMoveItemsAction,
} from '../../Store/Catalog/MoveItems/moveItems.actions.js';
import { DIALOG_ADD_ELEMENT } from './Dialogs/dialogs.constants';

const navigableItemTypes = ['folder', 'case_type', 'object_type'];

/**
 * @param {*} selected
 * @param {*} beingMoved
 * @returns {Object}
 */
export const addRowData = (selected, beingMoved) => row => ({
  ...row,
  path: getPathToItem(row.id, row.type),
  isNavigable: navigableItemTypes.includes(row.type),
  icon: row.type,
  id: row.id,
  selected: selected.includes(row.id),
  beingMoved: beingMoved.some(item => item.id === row.id),
});

export const markSelected = selected => row => ({
  ...row,
  selected: selected.includes(row.id),
});

/**
 * @param {Array} rows
 * @param {Array} selected
 * @return {*}
 */
export const getRowById = (rows, selected) =>
  rows.find(row => row.id === selected);

/**
 * @param {Object} state
 * @param {Object} state.catalog
 * @param {Object} state.catalog.items
 * @param {Object} state.catalog.details
 * @return {Object}
 */
const mapStateToProps = ({
  catalog: { items, details, moveItems },
  route,
}) => ({
  catalogItem: details.data,
  currentFolderName: items.currentFolderName,
  detailView: items.detailView,
  items: items.items,
  loadingDetailView: details.state === AJAX_STATE_PENDING,
  loadingTable: items.state === AJAX_STATE_PENDING,
  selectedItems: items.selectedItems,
  moveItems: moveItems.items,
  showDetailView: details.showDetailView,
  currentFolderUUID: items.currentFolderUUID,
  movingItems: moveItems.state === AJAX_STATE_PENDING,
  inSearch: inSearch(route),
});

/**
 * @param {Function} dispatch
 * @param {Function} props
 * @param {Array} props.segments
 * @return {Object}
 */
const mapDispatchToProps = dispatch => {
  const dispatchInvoke = payload => dispatch(invoke(payload));
  const doNavigate = path => {
    dispatchInvoke({
      path,
    });
  };

  return {
    doNavigate,
    addElement: () =>
      dispatch(
        showDialog({
          type: DIALOG_ADD_ELEMENT,
        })
      ),
    fetchCatalogItem: () => dispatch(fetchCatalogItem()),
    clearMoveItems: () => dispatch(clearMoveItemsAction()),
    confirmMoveItems: payload => dispatch(confirmMoveItemsAction(payload)),
    invoke: dispatchInvoke,
    toggleItem: bindActionCreators(toggleCatalogItem, dispatch),
    toggleDetailView: bindActionCreators(toggleCatalogDetailView, dispatch),
  };
};

/**
 * @param {Object} stateProps
 * @param {Object} dispatchProps
 * @param {Object} ownProps
 * @return {Object}
 */
const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    selectedItems,
    showDetailView,
    items,
    catalogItem,
    currentFolderName,
    currentFolderUUID,
    moveItems,
  } = stateProps;

  const { doNavigate } = dispatchProps;
  const { t } = ownProps;
  const title = t('catalog:title');

  const rows = items.map(addRowData(selectedItems, moveItems));
  const selectedRow = getRowById(rows, selectedItems[0]) || {};
  const selectedCount = items.length ? selectedItems.length : 0;

  const detailInfoConfig = {
    2: {
      name: `${selectedCount} ${t('catalog:detailView:itemsSelected')}`,
      icon: 'select_all',
    },
    1: {
      ...filterProperties(selectedRow, 'name', 'icon'),
      ...filterProperties(
        catalogItem,
        'version',
        'type',
        'details',
        'relations'
      ),
    },
    0: {
      name: currentFolderName || title,
      icon: 'folder',
    },
  };

  const detailInfo = detailInfoConfig[Math.min(selectedCount, 2)];

  const columnTranslations = t('catalog:column', { returnObjects: true });
  const addColumnHeaders = column => ({
    ...column,
    label: columnTranslations[column.name],
  });
  const columns = [
    { name: 'selected', width: 50 },
    { name: 'icon', width: 75 },
    { name: 'name', width: 1 },
    { name: 'type', width: 200 },
  ].map(addColumnHeaders);

  const onRowNavigate = ({ isNavigable, path }) => {
    if (isNavigable) {
      doNavigate(path);
    }
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    columns,
    detailInfo,
    rows,
    onRowNavigate,
    selectedRow,
    selectedCount,
    showDetailView,
    confirmMoveItems() {
      dispatchProps.confirmMoveItems({
        currentFolderUUID,
        moveItems,
      });
    },
    t,
  };
};

/**
 * Connects {@link Catalog} with {@link i18next}, and
 * the store.
 * @return {ReactElement}
 */
const CatalogContainer = translate()(
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  )(Catalog)
);

export default CatalogContainer;
