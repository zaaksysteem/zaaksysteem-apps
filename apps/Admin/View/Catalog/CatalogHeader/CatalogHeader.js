import React from 'react';
import { catalogHeaderStyleSheet } from './CatalogHeader.style';
import SubAppHeader from '../../Shared/Header/SubAppHeader';
import ButtonBar from './../ButtonBar/ButtonBarContainer';
import { preventDefaultAndCall } from '../../../../library/preventDefaultAndCall';
import { withStyles, Breadcrumbs } from '@mintlab/ui';

/**
 * @reactProps {Object} classes
 * @reactProps {Array<Object>} breadcrumbs
 * @reactProps {Function} doNavigate
 * @reactProps {Function} toggleFilterAction
 * @reactProps {string} query
 * @reactProps {Function} exitFilterAction
 * @reactProps {Function} doFilterAction
 * @reactProps {Function} t
 * @return {ReactElement}
 */
const CatalogHeader = ({ classes, breadcrumbs, doNavigate }) => (
  <SubAppHeader>
    <div className={classes.headerWrapper}>
      <div className={classes.navigation}>
        <Breadcrumbs
          maxItems={4}
          items={breadcrumbs}
          onItemClick={preventDefaultAndCall(event => {
            doNavigate(event.currentTarget.getAttribute('href'));
          })}
          scope="catalog"
        />
      </div>

      <div className={classes.center} />

      <div className={classes.actions}>
        <ButtonBar />
      </div>
    </div>
  </SubAppHeader>
);

export default withStyles(catalogHeaderStyleSheet)(CatalogHeader);
