import { get } from '@mintlab/kitchen-sink';
import { AJAX_STATE_INIT } from '../../../library/redux/ajax/createAjaxConstants';
import { SESSION_FETCH } from './session.constants';

import { handleAjaxStateChange } from '../../../library/redux/ajax/handleAjaxStateChange';

const initialState = {
  data: {},
  state: AJAX_STATE_INIT,
};

/**
 * @param {Object} state
 * @param {Object} action
 * @return {Object}
 */
const fetchSuccess = (state, action) => {
  const data = get(action, 'payload.response.result.instance');
  return {
    ...state,
    data,
  };
};

/**
 * @param {Object} state
 * @param {Object} action
 * @return {Object}
 */
export function session(state = initialState, action) {
  const handleAjaxState = handleAjaxStateChange(SESSION_FETCH);

  switch (action.type) {
    case SESSION_FETCH.SUCCESS:
      return fetchSuccess(handleAjaxState(state, action), action);
    default:
      return state;
  }
}
