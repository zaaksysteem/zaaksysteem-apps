import { createAjaxConstants } from '../../../library/redux/ajax/createAjaxConstants';

export const SESSION_FETCH = createAjaxConstants('SESSION_FETCH');
export const SESSION_AUTH_LOGIN = 'SESSION:AUTH:LOGIN';
export const SESSION_AUTH_LOGOUT = 'SESSION:AUTH:LOGOUT';
