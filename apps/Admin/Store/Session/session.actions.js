import { createAjaxAction } from '../../../library/redux/ajax/createAjaxAction';
import {
  SESSION_FETCH,
  SESSION_AUTH_LOGIN,
  SESSION_AUTH_LOGOUT,
} from './session.constants';

const fetchAjaxAction = createAjaxAction(SESSION_FETCH);

/**
 * @return {Function}
 */
export const fetchSession = () =>
  fetchAjaxAction({
    url: '/api/v1/session/current',
    method: 'GET',
  });

/**
 * @param {*} payload
 * @return {Function}
 */
export const login = payload => ({
  type: SESSION_AUTH_LOGIN,
  payload,
});

/**
 * @param {*} payload
 * @return {Function}
 */
export const logout = payload => ({
  type: SESSION_AUTH_LOGOUT,
  payload,
});
