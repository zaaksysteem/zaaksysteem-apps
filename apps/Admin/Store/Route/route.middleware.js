import { ROUTE_INVOKE, ROUTE_RESOLVE } from './route.constants';
import { resolve } from './route.actions';
import { pushState, replaceState } from '../../../library/dom/history';
import { getSectionFromPath } from '../../library/section';
import { sectionChange } from '../App/app.actions';

const handleInvoke = (store, next, action) => {
  next(action);

  const { replace, path } = action.payload;

  const updateHistory = replace ? replaceState : pushState;
  updateHistory(path);
  store.dispatch(resolve(action.payload));
};

const handleRouteResolve = (store, next, action) => {
  const currentSection = getSectionFromPath(store.getState().route);
  const nextSection = getSectionFromPath(action.payload.path);

  next(action);

  if (currentSection !== nextSection) {
    store.dispatch(sectionChange(nextSection));
  }
};

export const routeMiddleware = store => next => action => {
  switch (action.type) {
    case ROUTE_INVOKE:
      return handleInvoke(store, next, action);
    case ROUTE_RESOLVE:
      return handleRouteResolve(store, next, action);
    default:
      return next(action);
  }
};

export default routeMiddleware;
