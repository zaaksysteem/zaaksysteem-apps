import { get } from '@mintlab/kitchen-sink';
import { handleAjaxStateChange } from '../../../../library/redux/ajax/handleAjaxStateChange';
import { AJAX_STATE_INIT } from '../../../../library/redux/ajax/createAjaxConstants';
import { USERS_FETCH_LIST } from './users.constants';

export const initialState = {
  state: AJAX_STATE_INIT,
  data: [],
};

const fetchUsersSuccess = (state, action) => {
  const entries = get(action, 'payload.response.json.entries', []);
  const data = entries.map(entry => {
    const uuid = entry.object.uuid;
    const label = entry.label;

    return {
      uuid,
      value: uuid,
      label,
    };
  });

  return {
    ...state,
    data,
  };
};

export function users(state = initialState, action) {
  const handleAjaxState = handleAjaxStateChange(USERS_FETCH_LIST);
  const newState = handleAjaxState(state, action);

  if (action.type === USERS_FETCH_LIST.SUCCESS) {
    return fetchUsersSuccess(newState, action);
  }

  return newState;
}
