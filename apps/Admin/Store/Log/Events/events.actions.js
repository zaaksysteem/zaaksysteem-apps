import { buildUrl } from '@mintlab/kitchen-sink';
import { createAjaxAction } from '../../../../library/redux/ajax/createAjaxAction';
import { getEventsQueryParams } from '../library/log.functions';
import {
  EVENTS_FETCH,
  EVENTS_SET_ROWS_PER_PAGE,
  EVENTS_SET_PAGE,
  EVENTS_EXPORT,
} from './events.contants';

const fetchAjaxAction = createAjaxAction(EVENTS_FETCH);

/**
 * @param {Object} options
 * @param {number} options.page
 * @param {number} options.rowsPerPage
 * @param {number} options.caseNumber
 * @param {string} options.keyword
 * @param {string} options.subject
 * @returns {Promise}
 */
export const eventsFetch = ({
  page = 1,
  rowsPerPage = 50,
  caseNumber,
  keyword,
  user,
} = {}) => {
  const queryParams = getEventsQueryParams({
    page,
    rowsPerPage,
    caseNumber,
    keyword,
    user,
  });
  const url = buildUrl('/api/v1/eventlog', queryParams);

  return fetchAjaxAction({
    url,
    method: 'GET',
    payload: {
      page,
      rowsPerPage,
    },
  });
};

export const eventsSetRowsPerPage = rowsPerPage => ({
  type: EVENTS_SET_ROWS_PER_PAGE,
  payload: {
    rowsPerPage,
  },
});

export const eventsSetPage = page => ({
  type: EVENTS_SET_PAGE,
  payload: {
    page,
  },
});

export const eventsExport = () => ({
  type: EVENTS_EXPORT,
});
