import { buildUrl, getSegments } from '@mintlab/kitchen-sink';
import { navigate } from './../../../library/url';
import { invoke, resolve } from '../Route/route.actions';
import {
  EVENTS_SET_PAGE,
  EVENTS_SET_ROWS_PER_PAGE,
  EVENTS_EXPORT,
} from './Events/events.contants';
import { eventsFetch, eventsSetPage } from './Events/events.actions';
import { FILTERS_APPLY } from './Filters/filters.constants';
import { fetchUserLabel, filtersApply } from './Filters/filters.actions';
import {
  shouldRedirect,
  shouldFetchEvents,
  shouldFetchUserLabel,
  getEventsQueryParams,
  urlFiltersMatchState,
  getFiltersFromPath,
  isCompleteLogPath,
} from './library/log.functions';

/**
 * Build the URL from state and invoke a route
 *
 * @param {Object} store
 * @param {Function} next
 * @param {Object} action
 */
export const triggerLogRoute = (store, next, action) => {
  next(action);

  const state = store.getState();
  const {
    log: {
      filters: { keyword, user, caseNumber },
      events: { page, rowsPerPage },
    },
    route,
  } = state;

  const url = buildUrl(`/admin/logboek/${page}/${rowsPerPage}`, {
    keyword,
    caseNumber,
    user: user.uuid,
  });

  const routeAction = url === route ? resolve : invoke;

  store.dispatch(
    routeAction({
      path: url,
    })
  );
};

/**
 * @param {Object} store
 * @param {Function} next
 * @param {Object} action
 * @returns {Object}
 */
export const resetPageToInitial = (store, next, action) => {
  // First update filters in store
  next(action);
  return store.dispatch(eventsSetPage(1));
};

/**
 * Fetch events based on current URL
 *
 * @param {Object} store
 * @param {Function} next
 * @param {Object} action
 */
export const fetchEvents = (store, next, action) => {
  next(action);

  const { log, route } = store.getState();
  const [, , page, rowsPerPage] = getSegments(route);

  const {
    filters: { keyword, user, caseNumber },
  } = log;

  store.dispatch(
    eventsFetch({
      page,
      rowsPerPage,
      keyword,
      user,
      caseNumber,
    })
  );
};

/**
 * Redirect when a partial Log url is encountered
 *
 * @param {Object} store
 * @param {Function} next
 * @param {Object} action
 * @returns {Object}
 */
const handleRouteInvoke = (store, next, action) =>
  shouldRedirect(action.payload.path)
    ? triggerLogRoute(store, next, action)
    : next(action);

/**
 * @param {Object} store
 * @param {Function} next
 * @param {Object} action
 */
const dispatchApplyFilters = (store, next, action) => {
  next(action);

  const { route } = store.getState();

  if (isCompleteLogPath(route)) {
    const filters = getFiltersFromPath(route);
    store.dispatch(filtersApply(filters));
  }
};

/**
 * @param {Object} store
 * @param {Function} next
 * @param {Object} action
 * @returns {Object}
 */
const fetchEventsOrSkip = (store, next, action) =>
  shouldFetchEvents(action.payload.path)
    ? fetchEvents(store, next, action)
    : next(action);

/**
 * Fetch user label and events
 *
 * @param {Object} store
 * @param {Function} next
 * @param {Object} action
 * @returns {Object}
 */
const handleRouteResolve = (store, next, action) => {
  const state = store.getState();
  const { path } = action.payload;

  if (shouldFetchUserLabel(state)) {
    store.dispatch(fetchUserLabel(state.log.filters.user.uuid));
  }

  return urlFiltersMatchState(state, path)
    ? fetchEventsOrSkip(store, next, action)
    : dispatchApplyFilters(store, next, action);
};

/**
 * Build export URL and Navigate
 *
 * @param {Object} store
 * @param {Function} next
 * @param {Object} action
 * @returns {Object}
 */
const handleEventsDownload = (store, next, action) => {
  const {
    log: {
      events: { page, rowsPerPage },
      filters: { caseNumber, keyword, user },
    },
  } = store.getState();

  const queryParams = getEventsQueryParams({
    page,
    rowsPerPage,
    caseNumber,
    keyword,
    user,
  });
  const url = buildUrl('/api/v1/eventlog/download', queryParams);

  navigate(url);

  return next(action);
};

/* eslint complexity: [2, 7] */
/**
 * @param {Object} store
 * @returns {Function}
 */
export const logMiddleware = store => next => action => {
  const { type } = action;

  switch (type) {
    case 'ROUTE:INVOKE':
      return handleRouteInvoke(store, next, action);

    case 'ROUTE:RESOLVE':
      return handleRouteResolve(store, next, action);

    case FILTERS_APPLY:
      return resetPageToInitial(store, next, action);

    case EVENTS_SET_PAGE:
    case EVENTS_SET_ROWS_PER_PAGE:
      return triggerLogRoute(store, next, action);

    case EVENTS_EXPORT:
      return handleEventsDownload(store, next, action);

    default:
      return next(action);
  }
};

export default logMiddleware;
