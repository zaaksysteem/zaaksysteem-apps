import { combineReducers } from 'redux';
import { users } from './Users/users.reducer';
import { events } from './Events/events.reducer';
import { filters } from './Filters/filters.reducer';

export const log = combineReducers({
  events,
  filters,
  users,
});

export default log;
