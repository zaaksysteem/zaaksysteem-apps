import { createAjaxConstants } from '../../../../library/redux/ajax/createAjaxConstants';

export const CATALOG_FOLDER_INIT = 'CATALOG:FOLDER:INIT';
export const CATALOG_FOLDER_SAVE = createAjaxConstants('CATALOG:FOLDER:SAVE');
