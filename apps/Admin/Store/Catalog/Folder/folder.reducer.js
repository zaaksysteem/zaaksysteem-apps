import { handleAjaxStateChange } from '../../../../library/redux/ajax/handleAjaxStateChange';
import { AJAX_STATE_INIT } from '../../../../library/redux/ajax/createAjaxConstants';
import { CATALOG_FOLDER_INIT, CATALOG_FOLDER_SAVE } from './folder.constants';
import { get } from '@mintlab/kitchen-sink';

export const initialState = {
  state: AJAX_STATE_INIT,
  savingState: AJAX_STATE_INIT,
  id: null,
};

const handleInitFolder = (state, action) => {
  const options = get(action, 'payload.options');
  const id = get(options, 'id', null);
  const name = get(options, 'name') || '';

  return {
    ...initialState,
    id,
    name,
  };
};

/* eslint complexity: [2, 8] */
export function folder(state = initialState, action) {
  const handleSaveAjaxState = handleAjaxStateChange(
    CATALOG_FOLDER_SAVE,
    'savingState'
  );

  switch (action.type) {
    case CATALOG_FOLDER_INIT:
      return handleInitFolder(state, action);
    case CATALOG_FOLDER_SAVE.PENDING:
    case CATALOG_FOLDER_SAVE.ERROR:
    case CATALOG_FOLDER_SAVE.SUCCESS:
      return handleSaveAjaxState(state, action);
    default:
      return state;
  }
}

export default folder;
