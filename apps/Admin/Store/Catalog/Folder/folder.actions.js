import { createAjaxAction } from '../../../../library/redux/ajax/createAjaxAction';
import { CATALOG_FOLDER_INIT, CATALOG_FOLDER_SAVE } from './folder.constants';
import { cloneWithout } from '@mintlab/kitchen-sink';

const saveAjaxAction = createAjaxAction(CATALOG_FOLDER_SAVE);

export const initFolder = payload => ({
  type: CATALOG_FOLDER_INIT,
  payload,
});

export const saveFolder = payload => {
  const url = payload.isNew
    ? '/api/v2/admin/catalog/create_folder'
    : '/api/v2/admin/catalog/rename_folder';

  const data = cloneWithout(payload, 'isNew');

  return saveAjaxAction({
    url,
    method: 'POST',
    data,
  });
};
