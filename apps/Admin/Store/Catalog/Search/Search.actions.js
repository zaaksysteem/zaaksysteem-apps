import { CATALOG_SEARCH_EXIT, CATALOG_SEARCH } from './Search.constants';

export const doCatalogSearch = query => ({
  type: CATALOG_SEARCH,
  payload: {
    query,
  },
});

export const doExitSearch = payload => ({
  type: CATALOG_SEARCH_EXIT,
  payload,
});
