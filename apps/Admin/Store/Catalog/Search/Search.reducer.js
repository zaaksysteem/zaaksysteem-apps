import { get, objectifyParams } from '@mintlab/kitchen-sink';
import { ROUTE_RESOLVE } from '../../Route/route.constants';

export const initialState = {
  query: null,
};

const handleRouteResolve = (state, action) => {
  const { path } = action.payload;
  const splitPath = path.split('?');
  const query = get(objectifyParams(splitPath[splitPath.length - 1]), 'query');

  return {
    ...state,
    query,
  };
};

export function filter(state = initialState, action) {
  const { type } = action;

  switch (type) {
    case ROUTE_RESOLVE:
      return handleRouteResolve(state, action);
    default:
      return state;
  }
}

export default filter;
