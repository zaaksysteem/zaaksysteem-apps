import { createAjaxAction } from '../../../../library/redux/ajax/createAjaxAction';
import {
  CATALOG_ATTRIBUTE_INIT,
  CATALOG_ATTRIBUTE_FETCH,
  CATALOG_ATTRIBUTE_SAVE,
  CATALOG_ATTRIBUTE_INTEGRATIONS_FETCH,
} from './attribute.constants';
import { cloneWithout, buildUrl } from '@mintlab/kitchen-sink';

const fetchAjaxAction = createAjaxAction(CATALOG_ATTRIBUTE_FETCH);
const fetchIntegrationsAjaxAction = createAjaxAction(
  CATALOG_ATTRIBUTE_INTEGRATIONS_FETCH
);
const saveAjaxAction = createAjaxAction(CATALOG_ATTRIBUTE_SAVE);

export const initAttribute = payload => ({
  type: CATALOG_ATTRIBUTE_INIT,
  payload,
});

export const fetchAttribute = payload => {
  const attribute_id = payload;

  return fetchAjaxAction({
    url: buildUrl('/api/v2/admin/catalog/get_attribute_detail', {
      attribute_id,
    }),
    method: 'GET',
  });
};

export const fetchAttributeIntegrations = () => {
  return fetchIntegrationsAjaxAction({
    url: buildUrl(
      '/api/v2/admin/integrations/get_active_appointment_integrations',
      {}
    ),
    method: 'GET',
  });
};

export const saveAttribute = payload => {
  const url = payload.isNew
    ? '/api/v2/admin/catalog/create_attribute'
    : '/api/v2/admin/catalog/edit_attribute';

  const data = cloneWithout(payload, 'isNew');

  return saveAjaxAction({
    url,
    method: 'POST',
    data,
  });
};
