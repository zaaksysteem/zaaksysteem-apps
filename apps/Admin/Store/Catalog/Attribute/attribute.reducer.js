import { handleAjaxStateChange } from '../../../../library/redux/ajax/handleAjaxStateChange';
import { AJAX_STATE_INIT } from '../../../../library/redux/ajax/createAjaxConstants';
import {
  CATALOG_ATTRIBUTE_INIT,
  CATALOG_ATTRIBUTE_FETCH,
  CATALOG_ATTRIBUTE_SAVE,
  CATALOG_ATTRIBUTE_INTEGRATIONS_FETCH,
} from './attribute.constants';
import { get, asArray } from '@mintlab/kitchen-sink';

export const initialState = {
  state: AJAX_STATE_INIT,
  savingState: AJAX_STATE_INIT,
  id: null,
};

const handleInitAttribute = (state, action) => {
  const id = get(action, 'payload.options.id', null);
  return {
    ...initialState,
    id,
  };
};

/**
 * Process incoming data from backend
 *
 * @param {Object} state
 * @param {Object} action
 * @return {*}
 */
const handleFetchSuccess = (state, action) => ({
  ...state,
  values: get(action, 'payload.response.data.attributes'),
});

const handleFetchIntegrationsSuccess = (state, action) => {
  const payload = get(action, 'payload.response.data');

  const integrations = asArray(payload)
    .filter(
      integration =>
        get(integration, 'attributes.active') &&
        get(integration, 'attributes.module') === 'appointment'
    )
    .map(filteredIntegrations => ({
      uuid: filteredIntegrations.id,
    }));
  return {
    ...state,
    appointment_interface_uuid: get(integrations, '[0].uuid'),
  };
};

/* eslint complexity: [2, 12] */
export function attribute(state = initialState, action) {
  const handleFetchAjaxState = handleAjaxStateChange(CATALOG_ATTRIBUTE_FETCH);
  const handleSaveAjaxState = handleAjaxStateChange(
    CATALOG_ATTRIBUTE_SAVE,
    'savingState'
  );
  const handleFetchIntegrationsAjaxState = handleAjaxStateChange(
    CATALOG_ATTRIBUTE_INTEGRATIONS_FETCH,
    'integrationsState'
  );

  switch (action.type) {
    case CATALOG_ATTRIBUTE_INIT:
      return handleInitAttribute(state, action);
    case CATALOG_ATTRIBUTE_FETCH.SUCCESS:
      return handleFetchSuccess(handleFetchAjaxState(state, action), action);
    case CATALOG_ATTRIBUTE_FETCH.PENDING:
    case CATALOG_ATTRIBUTE_FETCH.ERROR:
      return handleFetchAjaxState(state, action);
    case CATALOG_ATTRIBUTE_SAVE.PENDING:
    case CATALOG_ATTRIBUTE_SAVE.ERROR:
    case CATALOG_ATTRIBUTE_SAVE.SUCCESS:
      return handleSaveAjaxState(state, action);
    case CATALOG_ATTRIBUTE_INTEGRATIONS_FETCH.PENDING:
    case CATALOG_ATTRIBUTE_INTEGRATIONS_FETCH.ERROR:
      return handleFetchIntegrationsAjaxState(state, action);
    case CATALOG_ATTRIBUTE_INTEGRATIONS_FETCH.SUCCESS:
      return handleFetchIntegrationsSuccess(
        handleFetchIntegrationsAjaxState(state, action),
        action
      );
    default:
      return state;
  }
}

export default attribute;
