import { createAjaxConstants } from '../../../../library/redux/ajax/createAjaxConstants';

export const CATALOG_CHANGE_ONLINE_STATUS = createAjaxConstants(
  'CATALOG:CHANGE_ONLINE_STATUS'
);
export const CATALOG_INIT_CHANGE_ONLINE_STATUS =
  'CATALOG:INIT:CHANGE_ONLINE_STATUS';
