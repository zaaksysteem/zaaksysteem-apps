import { createAjaxAction } from '../../../../library/redux/ajax/createAjaxAction';
import {
  CATALOG_CASE_TYPE_VERSIONS_INIT,
  CATALOG_CASE_TYPE_VERSIONS_FETCH,
  CATALOG_CASE_TYPE_VERSIONS_ACTIVATE_INIT,
  CATALOG_CASE_TYPE_VERSIONS_ACTIVATE,
} from './caseTypeVersions.constants';
import { buildUrl } from '@mintlab/kitchen-sink';

const fetchAjaxAction = createAjaxAction(CATALOG_CASE_TYPE_VERSIONS_FETCH);
const activateAjaxAction = createAjaxAction(
  CATALOG_CASE_TYPE_VERSIONS_ACTIVATE
);

export const initCaseTypeVersions = payload => ({
  type: CATALOG_CASE_TYPE_VERSIONS_INIT,
  payload,
});

export const fetchCaseTypeVersions = payload =>
  fetchAjaxAction({
    url: buildUrl('/api/v2/admin/catalog/get_case_type_history', {
      case_type_id: payload,
    }),
    method: 'GET',
  });

export const initCaseTypeVersionsActivate = payload => ({
  type: CATALOG_CASE_TYPE_VERSIONS_ACTIVATE_INIT,
  payload,
});

export const caseTypeVersionsActivate = ({
  case_type_id,
  version_id,
  reason,
}) =>
  activateAjaxAction({
    url: '/api/v2/admin/catalog/activate_case_type_version',
    method: 'POST',
    data: {
      case_type_id,
      reason,
      version_id,
    },
  });
