import { handleAjaxStateChange } from '../../../../library/redux/ajax/handleAjaxStateChange';
import { AJAX_STATE_INIT } from '../../../../library/redux/ajax/createAjaxConstants';
import {
  CATALOG_CASE_TYPE_VERSIONS_INIT,
  CATALOG_CASE_TYPE_VERSIONS_FETCH,
  CATALOG_CASE_TYPE_VERSIONS_ACTIVATE_INIT,
  CATALOG_CASE_TYPE_VERSIONS_ACTIVATE,
} from './caseTypeVersions.constants';
import { get } from '@mintlab/kitchen-sink';

export const initialState = {
  state: AJAX_STATE_INIT,
  activatingState: AJAX_STATE_INIT,
  id: null,
  idToActivate: null,
  versionToActivate: null,
  versions: [],
};

const handleInitCaseTypeVersions = (state, action) => ({
  ...initialState,
  id: get(action, 'payload.id', null),
});

const handleInitCaseTypeActivate = (state, action) => ({
  ...state,
  case_type_id: get(action, 'payload.case_type_id', null),
  version_id: get(action, 'payload.version_id', null),
  versionToActivate: get(action, 'payload.version', null),
});

const handleCaseTypeActivate = () => initialState;

/**
 * Process incoming data from backend
 *
 * @param {Object} state
 * @param {Object} action
 * @return {*}
 */
const handleFetchSuccess = (state, action) => ({
  ...state,
  versions: get(action, 'payload.response.data'),
});

/* eslint complexity: [2, 8] */
export function caseTypeVersions(state = initialState, action) {
  const handleFetchAjaxState = handleAjaxStateChange(
    CATALOG_CASE_TYPE_VERSIONS_FETCH
  );

  switch (action.type) {
    case CATALOG_CASE_TYPE_VERSIONS_INIT:
      return handleInitCaseTypeVersions(state, action);
    case CATALOG_CASE_TYPE_VERSIONS_FETCH.SUCCESS:
      return handleFetchSuccess(handleFetchAjaxState(state, action), action);
    case CATALOG_CASE_TYPE_VERSIONS_ACTIVATE_INIT:
      return handleInitCaseTypeActivate(state, action);
    case CATALOG_CASE_TYPE_VERSIONS_ACTIVATE:
      return handleCaseTypeActivate(state, action);
    default:
      return state;
  }
}

export default caseTypeVersions;
