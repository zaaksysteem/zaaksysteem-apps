import { buildUrl } from '@mintlab/kitchen-sink';
import { createAjaxAction } from '../../../../library/redux/ajax/createAjaxAction';
import {
  CATALOG_FETCH_DETAILS,
  CATALOG_TOGGLE_DETAIL_VIEW,
} from './details.constants';

const fetchItemAjaxAction = createAjaxAction(CATALOG_FETCH_DETAILS);

export const fetchCatalogItem = (type, id) =>
  fetchItemAjaxAction({
    url: buildUrl('/api/v2/admin/catalog/get_entry_detail', {
      type,
      item_id: id,
    }),
    method: 'GET',
  });

export const toggleCatalogDetailView = () => ({
  type: CATALOG_TOGGLE_DETAIL_VIEW,
});
