import { handleAjaxStateChange } from '../../../../library/redux/ajax/handleAjaxStateChange';
import { AJAX_STATE_INIT } from '../../../../library/redux/ajax/createAjaxConstants';
import {
  CATALOG_FETCH_DETAILS,
  CATALOG_TOGGLE_DETAIL_VIEW,
} from './details.constants';

export const fetchCatalogItemSuccess = (state, action) => {
  const { response } = action.payload;

  const { id, type, links, attributes, relationships } = response.data;

  const attributeConfig = {
    last_modified: 'lastModified',
    magic_string: 'magicString',
    value_type: 'valueType',
  };

  const detailAttributes = [
    'last_modified',
    'magic_string',
    'value_type',
    'identification',
  ]
    .filter(attr => Object.prototype.hasOwnProperty.call(attributes, attr))
    .map(detail => ({
      type: attributeConfig[detail] || detail,
      value: attributes[detail],
    }));

  if (type === 'case_type') {
    detailAttributes.push({
      type: 'id',
      value: id,
      link: `/api/v1/casetype/${id}`,
    });
  }

  if (type === 'document_template') {
    detailAttributes.push({
      type: 'enclosedDocument',
      value: attributes.filename,
      ...(attributes.has_default_integration && {
        link: links.download,
      }),
    });
  }

  const relations = Object.keys(relationships || {})
    .map(relationship => {
      const activeRelationNames = relationships[relationship]
        .filter(relation => relation.attributes.is_current_version)
        .map(relation => relation.attributes.name);

      return {
        type: relationship,
        value: activeRelationNames,
      };
    })
    .filter(relationship => relationship.value.length);

  const data = {
    details: detailAttributes,
    id,
    name: attributes.name,
    relations,
    type,
    url: attributes.url,
    version: attributes.current_version,
  };

  return {
    ...state,
    data,
  };
};

export const initialState = {
  state: AJAX_STATE_INIT,
  data: {},
  showDetailView: false,
};

export function details(state = initialState, action) {
  const handleAjaxState = handleAjaxStateChange(CATALOG_FETCH_DETAILS);

  switch (action.type) {
    case CATALOG_FETCH_DETAILS.PENDING:
    case CATALOG_FETCH_DETAILS.ERROR:
      return handleAjaxState(state, action);

    case CATALOG_FETCH_DETAILS.SUCCESS:
      return fetchCatalogItemSuccess(handleAjaxState(state, action), action);

    case CATALOG_TOGGLE_DETAIL_VIEW:
      return {
        ...state,
        showDetailView: !state.showDetailView,
      };

    default:
      return state;
  }
}

export default details;
