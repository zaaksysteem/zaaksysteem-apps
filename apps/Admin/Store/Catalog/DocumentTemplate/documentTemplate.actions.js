import { createAjaxAction } from '../../../../library/redux/ajax/createAjaxAction';
import {
  CATALOG_DOCUMENT_TEMPLATE_INIT,
  CATALOG_DOCUMENT_TEMPLATE_FETCH,
  CATALOG_DOCUMENT_TEMPLATE_SAVE,
  CATALOG_DOCUMENT_TEMPLATE_INTEGRATIONS_FETCH,
} from './documentTemplate.constants';
import { buildUrl } from '@mintlab/kitchen-sink';

const saveAjaxAction = createAjaxAction(CATALOG_DOCUMENT_TEMPLATE_SAVE);
const fetchAjaxAction = createAjaxAction(CATALOG_DOCUMENT_TEMPLATE_FETCH);
const fetchIntegrationsAjaxAction = createAjaxAction(
  CATALOG_DOCUMENT_TEMPLATE_INTEGRATIONS_FETCH
);

export const initDocumentTemplate = payload => ({
  type: CATALOG_DOCUMENT_TEMPLATE_INIT,
  payload,
});

export const fetchDocumentTemplate = payload => {
  const id = payload;

  return fetchAjaxAction({
    url: buildUrl('/api/v2/admin/catalog/get_document_template_detail', {
      document_template_id: id,
    }),
    method: 'GET',
  });
};

export const fetchDocumentTemplateIntegrations = () => {
  return fetchIntegrationsAjaxAction({
    url: buildUrl(
      '/api/v2/admin/integrations/get_active_document_integrations',
      {}
    ),
    method: 'GET',
  });
};

export const saveDocumentTemplate = payload => {
  const { documentTemplateId, isNew, fields } = payload;

  const url = isNew
    ? '/api/v2/admin/catalog/create_document_template'
    : '/api/v2/admin/catalog/edit_document_template';

  return saveAjaxAction({
    url,
    method: 'POST',
    data: {
      document_template_uuid: documentTemplateId,
      fields,
    },
  });
};
