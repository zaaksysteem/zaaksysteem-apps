import { createAjaxAction } from '../../../../library/redux/ajax/createAjaxAction';
import {
  CATALOG_EMAIL_TEMPLATE_INIT,
  CATALOG_EMAIL_TEMPLATE_FETCH,
  CATALOG_EMAIL_TEMPLATE_SAVE,
} from './emailTemplate.constants';
import { cloneWithout, buildUrl } from '@mintlab/kitchen-sink';

const fetchAjaxAction = createAjaxAction(CATALOG_EMAIL_TEMPLATE_FETCH);
const saveAjaxAction = createAjaxAction(CATALOG_EMAIL_TEMPLATE_SAVE);

export const initEmailTemplate = payload => ({
  type: CATALOG_EMAIL_TEMPLATE_INIT,
  payload,
});

export const fetchEmailTemplate = payload => {
  const email_template_id = payload;

  return fetchAjaxAction({
    url: buildUrl('/api/v2/admin/catalog/get_email_template_detail', {
      email_template_id,
    }),
    method: 'GET',
  });
};

export const saveEmailTemplate = payload => {
  const url = payload.isNew
    ? '/api/v2/admin/catalog/create_email_template'
    : '/api/v2/admin/catalog/edit_email_template';

  const data = cloneWithout(payload, 'isNew');

  return saveAjaxAction({
    url,
    method: 'POST',
    data,
  });
};
