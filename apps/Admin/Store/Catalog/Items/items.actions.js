import { buildUrl } from '@mintlab/kitchen-sink';
import { createAjaxAction } from '../../../../library/redux/ajax/createAjaxAction';
import {
  CATALOG_FETCH,
  CATALOG_TOGGLE_ITEM,
  CATALOG_CLEAR_SELECTED,
} from './items.constants';
import { queryToObject } from '../../../library/searchQuery';

const fetchAjaxAction = createAjaxAction(CATALOG_FETCH);

export const fetchCatalog = payload => {
  const { id, query } = payload;

  if (query) {
    const queryObject = queryToObject(query);
    return fetchAjaxAction({
      payload,
      url: buildUrl('/api/v2/admin/catalog/search', queryObject),
      method: 'GET',
    });
  }

  return fetchAjaxAction({
    payload,
    url: buildUrl('/api/v2/admin/catalog/get_folder_contents', {
      folder_id: id,
    }),
    method: 'GET',
  });
};

export const toggleCatalogItem = (id, multiSelect = false) => ({
  type: CATALOG_TOGGLE_ITEM,
  payload: {
    id,
    multiSelect,
  },
});

export const catalogClearSelected = () => ({
  type: CATALOG_CLEAR_SELECTED,
});
