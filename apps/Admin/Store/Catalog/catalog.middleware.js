import { batch } from 'react-redux';
import { catalogClearSelected } from './Items/items.actions';
import { fetchCatalog } from './Items/items.actions';
import { fetchCatalogItem } from './Details/details.actions';
import { CATALOG_TOGGLE_DETAIL_VIEW } from './Details/details.constants';
import { CATALOG_TOGGLE_ITEM, CATALOG_FETCH } from './Items/items.constants';
import {
  CATALOG_DUPLICATE_CASETYPE,
  CATALOG_EDIT_CASETYPE,
  CATALOG_EDIT_OBJECTTYPE,
  CATALOG_EXPORT_CASETYPE,
} from './ButtonBar/buttonBar.constants';
import {
  CATALOG_MOVEITEMS_CONFIRM,
  CATALOG_MOVEITEMS_START,
} from './MoveItems/moveItems.constants';
import {
  CATALOG_INIT_CHANGE_ONLINE_STATUS,
  CATALOG_CHANGE_ONLINE_STATUS,
} from './ChangeOnlineStatus/changeOnlineStatus.constants';
import {
  CATALOG_ATTRIBUTE_INIT,
  CATALOG_ATTRIBUTE_SAVE,
} from './Attribute/attribute.constants';
import {
  CATALOG_EMAIL_TEMPLATE_INIT,
  CATALOG_EMAIL_TEMPLATE_SAVE,
} from './EmailTemplate/emailTemplate.constants';
import {
  CATALOG_FOLDER_INIT,
  CATALOG_FOLDER_SAVE,
} from './Folder/folder.constants';
import {
  CATALOG_CASE_TYPE_VERSIONS_INIT,
  CATALOG_CASE_TYPE_VERSIONS_ACTIVATE_INIT,
  CATALOG_CASE_TYPE_VERSIONS_ACTIVATE,
} from './CaseTypeVersions/caseTypeVersions.constants';
import {
  CATALOG_DELETE_ITEM,
  CATALOG_REQUEST_DELETE_ITEM,
} from './DeleteItem/deleteItem.constants';

import {
  fetchAttribute,
  fetchAttributeIntegrations,
} from './Attribute/attribute.actions';
import { fetchEmailTemplate } from './EmailTemplate/emailTemplate.actions';
import { fetchCaseTypeVersions } from './CaseTypeVersions/caseTypeVersions.actions';

import {
  CATALOG_DOCUMENT_TEMPLATE_INIT,
  CATALOG_DOCUMENT_TEMPLATE_SAVE,
} from './DocumentTemplate/documentTemplate.constants';
import {
  fetchDocumentTemplate,
  fetchDocumentTemplateIntegrations,
} from './DocumentTemplate/documentTemplate.actions';

import { clearMoveItems } from './MoveItems/moveItems.actions';
import { get, getSegments, buildUrl } from '@mintlab/kitchen-sink';
import {
  getPathToCaseType,
  getPathToObjectType,
} from '../../library/catalog/pathGetters';
import { invoke } from './../Route/route.actions';
import { navigate } from './../../../library/url';
import { ROUTE_RESOLVE } from '../Route/route.constants';
import { showDialog, hideDialog } from '../UI/ui.actions';
import {
  DIALOG_ATTRIBUTE,
  DIALOG_EMAIL_TEMPLATE,
  DIALOG_FOLDER,
  DIALOG_CASE_TYPE_VERSIONS,
  DIALOG_CASE_TYPE_VERSIONS_ACTIVATE,
  DIALOG_CONFIRM_DELETE,
} from '../../View/Catalog/Dialogs/dialogs.constants';
import { CATALOG_SEARCH, CATALOG_SEARCH_EXIT } from './Search/Search.constants';
import { inSearch } from '../../library/catalog/inSearch';

const getFetchCatalogParams = store => {
  const { catalog } = store.getState();
  const id = get(catalog, 'items.currentFolderUUID', null);
  const query = get(catalog, 'search.query');

  if (query) {
    return { query };
  }
  return { id };
};

/**
 * @param {Object} store
 * @param {Function} next
 * @param {Object} action
 * @returns {Object}
 */
export const handleRouteResolve = (store, next, action) => {
  const { dispatch } = store;
  const path = get(action, 'payload.path');

  next(action);

  const segments = getSegments(path);
  const [, location, id] = segments;

  if (inSearch(path)) {
    return dispatch(fetchCatalog(getFetchCatalogParams(store)));
  }

  if (location === 'catalogus') {
    return dispatch(fetchCatalog({ id: id || null }));
  }

  return dispatch(clearMoveItems());
};

/**
 * @param {Object} store
 * @param {Function} next
 * @param {Object} action
 */
export const handleDetailView = (store, next, action) => {
  next(action);

  const { catalog } = store.getState();
  const {
    details: { showDetailView },
    items: { items, selectedItems },
  } = catalog;

  const selectedItemId = selectedItems[0];
  const selectedItem = items.find(item => item.id === selectedItemId);

  if (showDetailView && selectedItems.length === 1) {
    store.dispatch(fetchCatalogItem(selectedItem.type, selectedItemId));
  }
};

/**
 * @param {Object} store
 * @param {Function} next
 * @param {Object} action
 * @returns {Object}
 */
export const handleCaseTypeCall = (store, next, action) => {
  const { catalog } = store.getState();
  const id = get(catalog, 'items.selectedItems[0]');
  const urlTypes = {
    [CATALOG_DUPLICATE_CASETYPE]: getPathToCaseType(id, '/clone', true),
    [CATALOG_EDIT_CASETYPE]: getPathToCaseType(id, '/bewerken', true),
  };

  store.dispatch(
    invoke({
      path: urlTypes[action.type],
    })
  );

  return next(action);
};

/**
 * @param {Object} store
 * @param {Function} next
 * @param {Object} action
 * @returns {Object}
 */
export const handleObjectTypeCall = (store, next, action) => {
  const { catalog } = store.getState();
  const id = get(catalog, 'items.selectedItems[0]');
  const urlTypes = {
    [CATALOG_EDIT_OBJECTTYPE]: getPathToObjectType(id, '/bewerken', true),
  };

  store.dispatch(
    invoke({
      path: urlTypes[action.type],
    })
  );

  return next(action);
};

/**
 * @param {Object} store
 * @param {Function} next
 * @param {Object} action
 * @returns {Object}
 */
export const handleCaseTypeExport = (store, next, action) => {
  const { catalog } = store.getState();
  const id = get(catalog, 'items.selectedItems[0]');

  navigate(`/beheer/zaaktypen/${id}/export`);

  return next(action);
};

export const handleMoveConfirmSuccess = (store, next, action) => {
  next(action);

  const { dispatch } = store;

  batch(() => {
    dispatch(clearMoveItems());
    dispatch(fetchCatalog(getFetchCatalogParams(store)));
  });
};

/**
 * @param {Object} store
 * @param {Function} next
 * @param {Object} action
 * @returns {Object}
 */
const handleChangeOnlineStatusSuccess = (store, next, action) => {
  const { dispatch } = store;
  dispatch(hideDialog());

  handleDetailView(store, next, action);

  return next(action);
};

/**
 * @param {Object} store
 * @param {Function} next
 * @param {Object} action
 */
const handleInitChangeOnlineStatus = (store, next, action) => {
  next(action);
  store.dispatch(showDialog(action.payload));
};

/**
 * @param {Object} store
 * @param {Function} next
 * @param {Object} action
 */
const handleInitAttribute = (store, next, action) => {
  const payload = get(action, 'payload');
  const id = get(payload, 'options.id');
  const { dispatch } = store;

  next(action);

  batch(() => {
    if (id) {
      dispatch(fetchAttribute(id));
    }

    dispatch(fetchAttributeIntegrations());
    dispatch(showDialog({ ...payload, type: DIALOG_ATTRIBUTE }));
  });
};

/**
 * @param {Object} store
 * @param {Function} next
 * @param {Object} action
 * @returns {Object}
 */
const handleMoveItemsStart = (store, next, action) => {
  store.dispatch(catalogClearSelected());
  return next(action);
};

const handleAttributeSaveSucces = (store, next, action) => {
  next(action);
  const { dispatch } = store;

  batch(() => {
    dispatch(hideDialog());
    dispatch(fetchCatalog(getFetchCatalogParams(store)));
  });
};

/**
 * @param {Object} store
 * @param {Function} next
 * @param {Object} action
 */
const handleInitFolder = (store, next, action) => {
  const payload = get(action, 'payload');
  const { dispatch } = store;

  next(action);

  dispatch(showDialog({ ...payload, type: DIALOG_FOLDER }));
};

/**
 * @param {Object} store
 * @param {Function} next
 * @param {Object} action
 */
const handleInitEmailTemplate = (store, next, action) => {
  const payload = get(action, 'payload');
  const id = get(payload, 'options.id');
  const { dispatch } = store;

  next(action);

  batch(() => {
    if (id) {
      dispatch(fetchEmailTemplate(id));
    }

    dispatch(showDialog({ ...payload, type: DIALOG_EMAIL_TEMPLATE }));
  });
};

/**
 * @param {Object} store
 * @param {Function} next
 * @param {Object} action
 */
const handleEmailTemplateSaveSucces = (store, next, action) => {
  next(action);
  const { dispatch } = store;

  batch(() => {
    dispatch(hideDialog());
    dispatch(fetchCatalog(getFetchCatalogParams(store)));
  });
};

const handleFolderSaveSucces = (store, next, action) => {
  next(action);

  const { dispatch } = store;
  const { catalog } = store.getState();
  const {
    folder: { id, isNew },
  } = catalog;

  batch(() => {
    dispatch(hideDialog());

    if (isNew) {
      dispatch(
        invoke({
          path: `/admin/catalogus/${id}`,
        })
      );
    } else {
      dispatch(fetchCatalog(getFetchCatalogParams(store)));
    }
  });
};

const handleSearch = (store, next, action) => {
  const { dispatch } = store;
  const state = store.getState();
  const route = get(state, 'route');
  const [path] = route.split('?');
  const { query } = action.payload;

  dispatch(
    invoke({
      path: buildUrl(path, { query }),
      replace: inSearch(route),
    })
  );
};

const handleSearchExit = store => {
  const { dispatch } = store;
  const state = store.getState();
  const route = get(state, 'route');
  const [path] = route.split('?');

  dispatch(
    invoke({
      path,
    })
  );
};

const handleRequestDeleteItem = (store, next, action) => {
  next(action);
  store.dispatch(showDialog({ type: DIALOG_CONFIRM_DELETE }));
};

const handleDeleteSuccess = (store, next, action) => {
  next(action);

  const { dispatch } = store;

  batch(() => {
    dispatch(hideDialog());
    dispatch(fetchCatalog(getFetchCatalogParams(store)));
  });
};

/**
 * @param {Object} store
 * @param {Function} next
 * @param {Object} action
 */
const handleInitDocumentTemplate = (store, next, action) => {
  const payload = get(action, 'payload');
  const id = get(payload, 'options.id');
  const { dispatch } = store;

  next(action);

  if (id) {
    dispatch(fetchDocumentTemplate(id));
  }

  dispatch(fetchDocumentTemplateIntegrations());
  dispatch(showDialog({ ...payload, type: 'DocumentTemplate' }));
};

const handleDocumentTemplateSaveSuccess = (store, next, action) => {
  next(action);

  const { dispatch } = store;
  const { catalog } = store.getState();
  const currentFolderUUID = get(catalog, 'items.currentFolderUUID');
  dispatch(hideDialog());
  dispatch(fetchCatalog({ id: currentFolderUUID || null }));
};

/**
 * @param {Object} store
 * @param {Function} next
 * @param {Object} action
 */
const handleInitCaseTypeVersions = (store, next, action) => {
  const payload = get(action, 'payload');
  const id = get(payload, 'id');
  const { dispatch } = store;

  next(action);

  dispatch(fetchCaseTypeVersions(id));
  dispatch(showDialog({ ...payload, type: DIALOG_CASE_TYPE_VERSIONS }));
};

/**
 * @param {Object} store
 * @param {Function} next
 * @param {Object} action
 */
const handleInitCaseTypeActivate = (store, next, action) => {
  const payload = get(action, 'payload');
  const { dispatch } = store;

  next(action);

  dispatch(
    showDialog({ ...payload, type: DIALOG_CASE_TYPE_VERSIONS_ACTIVATE })
  );
};

/**
 * @param {Object} store
 * @param {Function} next
 * @param {Object} action
 */
const handleCaseTypeVersionsActivateSuccess = (store, next, action) => {
  const { dispatch } = store;

  handleDetailView(store, next, action);

  dispatch(hideDialog());
  dispatch(hideDialog());
};

/**
 * @param {Object} store
 * @param {Function} next
 * @param {Object} action
 * @returns {Object}
 */
/* eslint complexity: [2, 30] */
export const catalogMiddleware = store => next => action => {
  const { type } = action;

  switch (type) {
    case ROUTE_RESOLVE:
      return handleRouteResolve(store, next, action);

    case CATALOG_FETCH.SUCCESS:
    case CATALOG_TOGGLE_DETAIL_VIEW:
    case CATALOG_TOGGLE_ITEM:
      return handleDetailView(store, next, action);

    case CATALOG_DUPLICATE_CASETYPE:
    case CATALOG_EDIT_CASETYPE:
      return handleCaseTypeCall(store, next, action);

    case CATALOG_EDIT_OBJECTTYPE:
      return handleObjectTypeCall(store, next, action);

    case CATALOG_EXPORT_CASETYPE:
      return handleCaseTypeExport(store, next, action);

    case CATALOG_MOVEITEMS_START:
      return handleMoveItemsStart(store, next, action);

    case CATALOG_MOVEITEMS_CONFIRM.SUCCESS:
      return handleMoveConfirmSuccess(store, next, action);

    case CATALOG_REQUEST_DELETE_ITEM:
      return handleRequestDeleteItem(store, next, action);

    case CATALOG_DELETE_ITEM.SUCCESS:
      return handleDeleteSuccess(store, next, action);

    case CATALOG_CHANGE_ONLINE_STATUS.SUCCESS:
      return handleChangeOnlineStatusSuccess(store, next, action);

    case CATALOG_INIT_CHANGE_ONLINE_STATUS:
      return handleInitChangeOnlineStatus(store, next, action);

    case CATALOG_ATTRIBUTE_INIT:
      return handleInitAttribute(store, next, action);

    case CATALOG_ATTRIBUTE_SAVE.SUCCESS:
      return handleAttributeSaveSucces(store, next, action);

    case CATALOG_FOLDER_INIT:
      return handleInitFolder(store, next, action);

    case CATALOG_FOLDER_SAVE.SUCCESS:
      return handleFolderSaveSucces(store, next, action);

    case CATALOG_EMAIL_TEMPLATE_INIT:
      return handleInitEmailTemplate(store, next, action);

    case CATALOG_EMAIL_TEMPLATE_SAVE.SUCCESS:
      return handleEmailTemplateSaveSucces(store, next, action);

    case CATALOG_SEARCH:
      return handleSearch(store, next, action);

    case CATALOG_SEARCH_EXIT:
      return handleSearchExit(store, next, action);

    case CATALOG_CASE_TYPE_VERSIONS_INIT:
      return handleInitCaseTypeVersions(store, next, action);

    case CATALOG_CASE_TYPE_VERSIONS_ACTIVATE_INIT:
      return handleInitCaseTypeActivate(store, next, action);

    case CATALOG_CASE_TYPE_VERSIONS_ACTIVATE.SUCCESS:
      return handleCaseTypeVersionsActivateSuccess(store, next, action);

    case CATALOG_DOCUMENT_TEMPLATE_INIT:
      return handleInitDocumentTemplate(store, next, action);

    case CATALOG_DOCUMENT_TEMPLATE_SAVE.SUCCESS:
      return handleDocumentTemplateSaveSuccess(store, next, action);

    default:
      return next(action);
  }
};

export default catalogMiddleware;
