import { get, reduceMap, getSegments } from '@mintlab/kitchen-sink';
import { AJAX_STATE_INIT } from '../../../../library/redux/ajax/createAjaxConstants';

/**
 * @param {Object} options
 * @param {string} options.current
 * @param {string} options.next
 * @param {string} options.segment
 * @return {boolean}
 */
export const willSegmentChange = ({ currentPath, nextPath, segment }) => {
  const currentSegments = getSegments(currentPath);
  const nextSegments = getSegments(nextPath);

  const [, currentSegment] = currentSegments;
  const [, , currentCategory] = currentSegments;
  const [, nextSegment] = nextSegments;
  const [, , nextCategory] = nextSegments;

  const map = new Map([
    [() => currentSegment !== segment, () => false],
    [() => nextSegment !== segment, () => true],
    [() => nextCategory !== currentCategory, () => true],
  ]);

  return reduceMap({
    map,
    fallback: false,
  });
};

/**
 * @param {string} path
 * @returns {Boolean}
 */
export const pathIsInConfig = path =>
  path && path.indexOf('/admin/configuratie') > -1;

/**
 * @param {Object} state
 * @param {string} path
 * @returns {Boolean}
 */
export const shouldFetch = (state, path) =>
  pathIsInConfig(path) && state.systemConfiguration.state === AJAX_STATE_INIT;

/**
 * @param {Object} state
 * @param {string} path
 * @param {Boolean} force
 * @returns {Boolean}
 */
export const shouldDispatch = (state, path, force) => {
  const { route } = state;
  const segmentWillChange = willSegmentChange({
    currentPath: route,
    nextPath: path,
    segment: 'configuratie',
  });

  return (
    !force && segmentWillChange && get(state, 'ui.banners.systemConfigForm')
  );
};
