import { createReduxStore } from '../../library/redux/createStore';
import { route } from './Route/route.reducer';
import { ui } from './UI/ui.reducer';
import { catalog } from './Catalog';
import { log } from './Log';
import { systemConfiguration } from './SystemConfiguration/systemconfiguration.reducer';
import { session } from './Session/session.reducer';
import { app } from './App/app.reducer';
import middlewares from './middleware';
import { FEATURE_TOGGLE_CATALOG } from '../Fixtures/featureToggle';

const reducers = {
  app,
  route,
  ui,
  systemConfiguration,
  ...(FEATURE_TOGGLE_CATALOG ? { catalog } : {}),
  log,
  session,
};

/**
 * Create a store with initial state. Since the initial state
 * can be impure, this is a factory function that is called
 * from the application.
 *
 * @param {Object} initialState
 *   The initial state of the store.
 * @return {Store}
 */
export const createStore = initialState =>
  createReduxStore(reducers, initialState, middlewares);
