import * as fieldTypes from '../View/Shared/Form/Constants/fieldTypes';

const formDefinition = [
  {
    name: 'name',
    type: fieldTypes.TEXT,
    value: '',
    required: true,
    label: 'documentTemplate:fields.name.label',
    placeholder: 'documentTemplate:fields.name.label',
  },
  {
    name: 'integration_uuid',
    type: fieldTypes.FLATVALUE_SELECT,
    value: 'default',
    required: true,
    label: 'documentTemplate:fields.integration_uuid.label',
    placeholder: 'documentTemplate:fields.integration_uuid.label',
    hint: 'form:cannotChangeLater',
    choices: [
      {
        value: 'default',
        label: 'documentTemplate:fields.integration_uuid.defaultTypeLabel',
      },
    ],
  },
  {
    name: 'integration_reference',
    type: fieldTypes.TEXT,
    value: '',
    required: false,
    label: '',
  },
  {
    name: 'file',
    type: fieldTypes.FILE_SELECT,
    value: [],
    required: true,
    accept: ['.odt'],
    label: 'documentTemplate:fields.file.label',
    help: 'documentTemplate:fields.file.help',
    selectInstructions: 'files:selectInstructions',
    dragInstructions: 'files:dragInstructions',
    dropInstructions: 'files:dropInstructions',
    orLabel: 'files:orLabel',
    multiple: false,
  },
  {
    name: 'help',
    type: fieldTypes.TEXT,
    isMultiline: true,
    value: '',
    required: false,
    label: 'documentTemplate:fields.help.label',
    placeholder: 'documentTemplate:fields.help.label',
  },
  {
    name: 'commit_message',
    type: fieldTypes.TEXT,
    value: '',
    required: true,
    label: 'documentTemplate:fields.commit_message.label',
    help: 'documentTemplate:fields.commit_message.help',
  },
];

export default formDefinition;
