const formDefinition = [
  {
    name: 'reason',
    type: 'text',
    value: '',
    required: true,
    label: 'caseTypeVersions:fields.reason.label',
    placeholder: 'caseTypeVersions:fields.reason.label',
  },
];

export default formDefinition;
