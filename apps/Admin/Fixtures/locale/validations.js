export const validations = {
  validations: {
    mixed: {
      required: 'Vul een geldige waarde in.',
    },
    string: {
      invalidMagicString:
        'Deze Magic String is al in gebruik of ongeldig. Suggestie: {{suggestion}}.',
    },
    email: {
      invalidEmailAddress:
        'Er is geen geldig e-mailadres ingevuld. Suggestie: naam@example.com',
    },
    array: {
      noFile: 'Geen bestand(en) geüpload. Voeg een bestand toe.',
      invalidFile: 'Ongeldig(e) bestand(en) geüpload. Probeer het opnieuw.',
    },
  },
};
