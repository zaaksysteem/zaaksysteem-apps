export const server = {
  server: {
    errorTitle: 'Fout',
    status: {
      '401': 'U bent niet geauthoriseerd om deze actie uit te voeren.',
      '403': 'U heeft niet genoeg rechten om deze actie uit te voeren.',
      '404':
        'De opgevraagde pagina of gegevensbron kon niet worden gevonden. Probeer het later opnieuw.',
    },
    keys: {
      'case_type/not_found': 'Het zaaktype kon niet worden gevonden.',
      'case_type/status_already_true': 'Dit zaaktype is al online gezet.',
      'case_type/status_already_false': 'Dit zaaktype is al offline gezet.',
      'folder_entry/not_found': 'De item(s) konden niet worden gevonden.',
      'folder/not_found': 'De map kon niet worden gevonden.',
      "folder_entry/can't_move_to_child":
        'De item(s) kunnen niet naar deze map verplaatst worden.',
      'attribute/no_active_appointment_integrations':
        'Er konden geen actieve Kalender-koppelingen worden gevonden.',
      'attribute/not_found':
        'Het kenmerk met de opgegeven UUID kon niet worden gevonden.',
      'attribute/already_exists_with_uuid':
        'Een kenmerk met de opgegeven UUID bestaat al.',
      'attribute/invalid_magic_string':
        'De opgegeven Magic String is reeds in gebruik.',
      'attribute/missing_required_field':
        'Een verplicht veld is niet opgegeven.',
      'attribute/invalid_attribute_type':
        'Het opgegeven kenmerktype is ongeldig.',
      'bibliotheek_category/not_found':
        'De categorie met de opgegeven UUID kon niet worden gevonden.',
      'attribute/magic_string_cannot_be_generated':
        'Er kon geen geldige Magic String worden gegenereerd.',
      'folder/name_in_use':
        'Een map met de opgegeven naam bestaat al. Geef een andere naam op.',
      'document_template/not_found': 'Het sjabloon kon niet worden gevonden.',
      'document_template/already_exists_with_uuid':
        'Een sjabloon met het opgegeven UUID bestaat al.',
      'document_template/incorrect_template_name':
        'Ongeldige referentie opgegeven.',
      'folder/not_empty':
        'De map kan niet worden verwijderd omdat deze niet leeg is.',
      'case_type/used_in_cases':
        'Dit zaaktype kan niet verwijderd worden. Het wordt nog gebruikt door een bestaande zaak.',
      'case_type/used_in_case_types':
        'Dit zaaktype kan niet verwijderd worden. Het wordt nog gebruikt door een bestaand zaaktype.',
      'attribute/used_in_case_types':
        'Het attribuut kan niet verwijderd worden. Het wordt nog gebruikt door een bestaand zaaktype.',
      'email_template/used_in_case_types':
        'Het e-mailsjabloon kan niet verwijderd worden. Het wordt nog gebruikt door een bestaand zaaktype.',
      'object_type/used_in_case_types':
        'Dit objecttype kan niet verwijderd worden. Het wordt nog gebruikt door een bestaand zaaktype.',
      'document_template/used_in_case_types':
        'Dit documentsjabloon kan niet verwijderd worden. Het wordt nog gebruikt door een bestaand zaaktype.',
    },
    error: 'Er is een fout op de server opgetreden. Probeer het later opnieuw.',
  },
};

export default server;
