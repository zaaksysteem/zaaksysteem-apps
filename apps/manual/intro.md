# Introduction

## Goals

The main goal of our new apps is to be multi-tiered, where every tier
should have clear responsibilities and boundaries.

See the 
[Admin app tiers](app-admin.html#tiers) 
for more information about the current strategy of 
*Resource*, *Store* and *View*.
 
If needed, a tier must be replaceable with minimum impact. As an
example, replace *React* with *Vue* in the *View tier*, 
or replace *Redux* with *React Automata* in the *Store tier*.

This also includes build tools and the like. We don't want a tight
coupling with *any* framework ecosystem beyond its primary use case.

## History

The current *Admin* app started as a lean 
[Preact](https://preactjs.com/)
app in the main `zaaksysteem` repository. That part was later 
migrated to the dedicated `zaaksysteem-apps` repository as 
the first app of several to come.
