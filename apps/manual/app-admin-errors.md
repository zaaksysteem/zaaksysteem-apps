# Admin: Error handling

There are two general levels of error handling once the entry point HTML file could be resolved by the web server. The current display in the GUI is an MVP, i.e. better than an empty page.

## The entire app fails to load; maybe

- the browser is not supported
- the assets contain a build error
- a critical asset could no be resolved

That error handling, no matter its logic and presentation, is included in the body of the entry point HTML file.

## The React app was mounted but encountered an internal error

- the top level resource resolver spawned an error
- there was an error on the view level

See `ErrorBoundary` in [App Hierarchy](./app-admin-hierarchy.html)
