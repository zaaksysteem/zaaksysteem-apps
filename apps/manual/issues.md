# Known issues

- the `acorn` 
  [*build context* dependency](package.html) 
  should not be necessary but without it 
  `npm install --no-optional` leaves `eslint`
  in an unusable state
- the `zs start` command needs to be refactored once another
  app is added; once it doesn't default to `admin` anymore, 
  the `edge` parameter will conflict with the app name
- the `webpack-concat-plugin` uses a deprecated API
- `webpack-serve` has been deprecated and `webpack-dev-server`
  has been 'undeprecated' (if that *is* a word, it shouldn't) 
  after the fact
- using `/node/bin/library/constants.js` can result in 
  circular depndencies; that's bad design and should 
  be refactored
