import { request } from '../fetch';
import { get } from '@mintlab/kitchen-sink';

/**
 * These functions return API response data as a promise,
 * and are used inside the Attribute create/edit screen in the Catalog.
 *
 * API data coming into the form while the user is interacting with it,
 * such as async validation checks or options for a dropdown, are
 * considered temporary, and are not Redux actions.
 */

export const generateMagicString = string_input =>
  request(
    'GET',
    `/api/v2/admin/catalog/generate_magic_string?string_input=${encodeURIComponent(
      string_input
    )}`
  )
    .catch(() => Promise.reject(false))
    .then(requestPromise => requestPromise.json())
    .then(response => get(response, 'data.magic_string'));

export const getLocationList = uuid =>
  request('GET', `/api/v1/sysin/interface/${uuid}/trigger/get_location_list`)
    .catch(() => {})
    .then(requestPromise => requestPromise.json())
    .then(response => get(response, 'result.instance.data'));

export const getProductList = ({ uuid, location_id }) =>
  request(
    'GET',
    `/api/v1/sysin/interface/${uuid}/trigger/get_product_list?location_id=${location_id}`
  )
    .catch(() => {})
    .then(requestPromise => requestPromise.json())
    .then(response => get(response, 'result.instance.data'));
