import { request } from '../fetch';
import { get } from '@mintlab/kitchen-sink';

export const sendFile = file => {
  let formData = new FormData();

  formData.append('file', file);

  return request('POST', `/api/v2/file/create_file`, formData, 'formdata')
    .then(requestPromise => requestPromise.json())
    .then(response => get(response, 'data'))
    .catch(response => Promise.reject(response));
};
