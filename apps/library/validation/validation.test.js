import validate from './validation';

describe('The `validate` function', () => {
  test('returns undefined is no constraints are given', () => {
    expect(validate('test@test.com', [])).toBe(undefined);
  });

  test('returns undefined if all validation functions pass', () => {
    expect(validate('test@test.com', ['required', 'email'])).toBe(undefined);
  });

  test('ignores constraints for which there is no matching function', () => {
    expect(validate('test@test.com', ['email', 'NONEXISTANT'])).toBe(undefined);
  });

  test('returns the name of the failed validation in case of a failed validation', () => {
    expect(validate('invalid', ['required', 'email'], null)).toBe('email');
  });

  test('returns the name of the failed validation in case of a failed validation, in any order', () => {
    expect(validate('invalid', ['email', 'required'], null)).toBe('email');
  });

  test('returns the name of the validation if `undefined` was passed as the value', () => {
    expect(validate(undefined, ['uri'], null)).toBe('uri');
  });
});
