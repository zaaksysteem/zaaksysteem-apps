import * as constraintsMap from './constraints';
import { isDefined } from '../../library/value';
import * as yup from 'yup';

/**
 * ---------------------------------------
 *  OLD, ONLY USED IN SYSTEM CONFIGURATION
 * ---------------------------------------
 *
 * Calls a validation function (if it exists) with the same name for every entry
 * in the constraints array, with a value and optional configuration parameters.
 *
 * If no value is provided, or if the value does not pass validation, return the
 * name of the constraint.
 *
 * @param {*} value
 * @param {Array} constraints Array of strings, i.e. ['required', 'email']
 * @param {Object} config optional additional parameters
 * @return {string|undefined}
 */
const validate = (value, constraints = [], config) => {
  const filteredConstraints = constraints.filter(key => constraintsMap[key]);

  const valid = constraint =>
    isDefined(value) && constraintsMap[constraint](value, config);

  for (const constraint of filteredConstraints) {
    if (!valid(constraint)) {
      return constraint;
    }
  }
};

/**
 * Creates a Yup schema based on the provided formDefinition,
 * validationMap and translation function. If a field is hidden
 * or disabled, a given rule will not be added.
 *
 * This schema is then validated against the provided form values.
 *
 * Returns a promise that resolves with the yupErrors object.
 *
 * @param {Array} formDefinition
 * @param {Object} values
 * @param {Object} validationMap
 * @param {Function} t
 * @return {Promise}
 **/
export const createValidation = ({
  formDefinition,
  values,
  validationMap,
  t,
}) => {
  yup.setLocale({
    mixed: t('validations:mixed', { returnObjects: true }),
    string: t('validations:string', { returnObjects: true }),
  });

  const rule = field => {
    if (field.hidden || field.disabled) {
      return;
    }

    if (validationMap[field.type]) {
      return validationMap[field.type]({ t, field });
    }

    return field.required ? yup.string().required() : yup.string();
  };

  const schema = formDefinition.reduce((accumulator, field) => {
    const resolvedRule = rule(field);

    if (resolvedRule) {
      accumulator[field.name] = resolvedRule;
    }

    return accumulator;
  }, {});

  return yup
    .object()
    .shape(schema)
    .validate(values, { abortEarly: false })
    .then(() => Promise.resolve({}))
    .catch(yupErrors => Promise.resolve(yupErrors));
};

export default validate;
